﻿BASE.require([
		"jQuery",
        "app.model.TableTennisDisplay",
        "app.model.DefaultDisplayService",
        "app.model.EdmGeneratorService"
], function () {

    var TableTennisDisplay = app.model.TableTennisDisplay;
    var EdmGeneratorService = app.model.EdmGeneratorService;
    var DefaultDisplayService = app.model.DefaultDisplayService;

    BASE.namespace("app.components");

    app.components.Main = function (elem, tags, services) {
        var self = this;
        var $elem = $(elem);
        var database = $(tags["database"]).controller();
        var service = services.get("dataService");
        var edmGeneratorService = new EdmGeneratorService();
        var defaultDisplayService = new DefaultDisplayService(edmGeneratorService);
        var displayService = new TableTennisDisplay(service);

        database.setDisplayServiceAsync(defaultDisplayService).try();
        window.service = service;
    };
});