﻿BASE.require([
		"jQuery",
        "Date.prototype.format"
], function () {
    var Future = BASE.async.Future;
    BASE.namespace("app.components.inputs");

    var defaultValidateAsync = function () {
        return Future.fromResult();
    };
    var defaultMap = function (value) {
        return value;
    };

    app.components.inputs.OneToOneInput = function (elem, tags, services) {
        var self = this;
        var $elem = $(elem);
        var $label = $(tags["label"]);
        var $editButton = $(tags["edit-button"]);
        var $description = $(tags["description"]);
        var property = null;
        var map = defaultMap;
        var validateAsync = defaultValidateAsync;
        var oneToOneFormFuture = null;
        var relationship = null;
        var propertyName = null;
        var displayService = null;
        var value = null;
        var entity = null;

        var getOneToOneModal = function () {
            if (oneToOneFormFuture == null) {
                return entityFormFuture = services.get("windowService").createModalAsync({
                    componentName: "app/components/database/OneToOneForm.html",
                    height: 500,
                    width: 400
                })
            }

            return oneToOneFormFuture;
        };

        var setLabel = function (label) {
            $label.text(label);
        };

        var setDescription = function () {
            if (value == null) {
                $editButton.text("Add");
                $description.text("(None)");
            } else {
                $editButton.text("Edit");
                $description.text(displayService.getDisplayByType(relationship.ofType).displayInstance(value));
            }
        };

        self.setConfig = function (config) {
            entity = config.entity;
            Type = entity.constructor;
            propertyName = config.propertyName;
            displayService = config.displayService;
            property = displayService.getDisplayByType(Type).properties[propertyName];
            primaryKeyPropertyName = displayService.service.getEdm().getPrimaryKeyProperties(Type)[0];
            relationship = displayService.getRelationshipsByType(Type)[config.propertyName];

            //TODO: We need to check if the entity needs to be loaded if its null.
            value = entity[config.propertyName];
            setLabel(property.label());
            setDescription();
        };

        self.saveAsync = function () { };

        self.validateAsync = function () {
            return validateAsync(value).catch(function (error) {
                $error.text(error.message);
            });
        };

        $editButton.on("click", function () {
            getOneToOneModal().chain(function (windowManager) {
                var controller = windowManager.controller;
                var element = $(windowManager.element).addClass("absolute-fill-parent");

                controller.setConfigAsync({
                    relationship: relationship,
                    displayService: displayService,
                    entity: entity[propertyName] || new relationship.ofType(),
                    parentEntity: entity,
                    propertyName: propertyName
                }).chain(function (childEntity) {
                    entity[propertyName] = childEntity;
                    value = childEntity;
                    setDescription();
                    return windowManager.window.closeAsync();
                }).try();

                return windowManager.window.showAsync();

            }).try();
        });
    };
});