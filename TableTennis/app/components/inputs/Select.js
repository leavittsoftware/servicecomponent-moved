﻿BASE.require([
		"jQuery"
], function () {
    var Future = BASE.async.Future;
    var defaultValidateAsync = function () {
        return Future.fromResult();
    };
    var defaultMap = function (value) {
        return value;
    };


    BASE.namespace("app.components.inputs");

    app.components.inputs.Select = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $label = $(tags["label"]);
        var $input = $(tags["input"]);
        var $error = $(tags["error-message"]);
        var config = null;
        var map = defaultMap;
        var validateAsync = defaultValidateAsync;
        var value = null;
        var property = null;
        var Type = null;
        var entity = null;
        var displayService = null;
        var propertyName = null;
        var options = null;

        var setValue = function (value) {
            $input.val(value);
        };

        var setLabel = function (label) {
            $label.text(label);
        };

        var createOption = function (option) {
            var optionElement = document.createElement("option");
            optionElement.innerText = option.label;
            optionElement.value = option.label;
            return optionElement;
        };

        var createOptions = function (options) {
            var select = document.createElement("select");
            var $newinput = $(select);
            $newinput.addClass("xp-input");
            $newinput.css("width", "100%");

            $input.replaceWith($newinput);
            $input = $newinput;

            options.forEach(function (option) {
                $input.append(createOption(option));
            });

            if (options.length > 0) {
                $input.val(options[0].label);
            }
        };

        self.focus = function () {
            return $input.select();
        };

        self.getValue = function () {
            var value = $input.val();

            var actualValue = options.filter(function (option) {
                return option.label === value;
            })[0].value;

            return map(actualValue);
        };

        self.setConfig = function (config) {
            entity = config.entity;
            propertyName = config.propertyName;
            Type = config.Type;
            displayService = config.displayService;
            property = displayService.getDisplayByType(Type).properties[propertyName];
            value = entity[config.propertyName];
            options = property.inputComponent.options || [];

            createOptions(options);
            setValue(value);
            setLabel(property.label());
        };

        self.saveAsync = function () {
            entity[propertyName] = self.getValue();
        };

        self.validateAsync = function () {
            return validateAsync(self.getValue()).catch(function (error) {
                $error.text(error.message);
            });
        };
    };
});