﻿BASE.require([
		"jQuery",
        "Date.prototype.format",
        "Array.prototype.asQueryable",
        "BASE.data.DataContext"
], function () {
    var Future = BASE.async.Future;
    var DataContext = BASE.data.DataContext;

    BASE.namespace("app.components.inputs");

    var defaultValidateAsync = function () {
        return Future.fromResult();
    };
    var defaultMap = function (value) {
        return value;
    };

    app.components.inputs.OneToManyTargetInput = function (elem, tags, services) {
        var self = this;
        var $elem = $(elem);
        var $label = $(tags["label"]);
        var $description = $(tags["description"]);
        var $selectButton = $(tags["select-button"]);
        var property = null;
        var map = defaultMap;
        var validateAsync = defaultValidateAsync;
        var value = null;
        var Type = null;
        var entity = null;
        var displayService = null;
        var primaryKeyPropertyName = "id";
        var relationship = null;
        var propertyName = null;
        var oneToManyTargetFormFuture = null;

        var getOneToManyTargetFormModal = function () {
            if (oneToManyTargetFormFuture == null) {
                return oneToManyTargetFormFuture = services.get("windowService").createModalAsync({
                    componentName: "app/components/database/OneToManyTargetForm.html",
                    height: 500,
                    width: 700
                })
            }

            return oneToManyTargetFormFuture;
        };

        var setLabel = function (label) {
            $label.text(label);
        };

        var getValue = function () {
            return map(value);
        };

        var setDescription = function () {
            if (value == null) {
                $description.text("None");
                $selectButton.text("Select");
            } else {
                $description.text(displayService.getDisplayByType(relationship.type).displayInstance(value));
                $selectButton.text("Change");
            }
        };

        self.setConfig = function (config) {
            entity = config.entity;
            propertyName = config.propertyName;
            Type = entity.constructor;
            displayService = config.displayService;
            property = displayService.getDisplayByType(Type).properties[propertyName];
            primaryKeyPropertyName = displayService.service.getEdm().getPrimaryKeyProperties(Type)[0];
            relationship = displayService.getRelationshipsByType(Type)[propertyName];
            value = entity[propertyName];

            setLabel(property.label());
            setDescription();
        };

        self.saveAsync = function () {
            entity[propertyName] = getValue();
        };

        self.validateAsync = function () {
            return validateAsync(entity[propertyName]).catch(function (error) {
                $error.text(error.message);
            });
        };

        $selectButton.on("click", function () {
            getOneToManyTargetFormModal().chain(function (windowManager) {
                var controller = windowManager.controller;
                var element = $(windowManager.element).addClass("absolute-fill-parent");

                var futureValue = controller.setConfigAsync({
                    relationship: relationship,
                    displayService: displayService,
                    entity: entity,
                    propertyName: propertyName
                }).chain(function (childEntity) {
                    value = childEntity;
                    entity[propertyName] = childEntity;
                    setDescription();
                });

                futureValue.try();

                return windowManager.window.showAsync();

            }).try();
        });
    };
});