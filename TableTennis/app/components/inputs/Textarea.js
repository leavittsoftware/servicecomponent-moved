﻿BASE.require([
		"jQuery",
        "Date.prototype.format"
], function () {
    var Future = BASE.async.Future;
    BASE.namespace("app.components.inputs");

    var defaultValidateAsync = function () {
        return Future.fromResult();
    };
    var defaultMap = function (value) {
        return value;
    };

    app.components.inputs.Textarea = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $label = $(tags["label"]);
        var $input = $(tags["input"]);
        var $error = $(tags["error-message"]);
        var config = null;
        var map = defaultMap;
        var validateAsync = defaultValidateAsync;

        self.setValue = function (value) {
            $input.val(value);
        };

        self.focus = function () {
            return $input.select();
        };

        self.getValue = function () {
            return map($input.val());
        };

        self.setLabel = function (label) {
            $label.text(label);
        };

        self.setProperty = function (value) {
            property = value;
            validateAsync = value.inputComponent.validateAsync || defaultValidateAsync;
            self.setLabel(property.label());
        };

        self.validateAsync = function () {
            return validateAsync(self.getValue()).catch(function (error) {
                $error.text(error.message);
            });
        };

        $input.on("keydown", function (event) {
            if (event.keyCode === 13) {
                event.stopPropagation();
            }
        });
    };
});