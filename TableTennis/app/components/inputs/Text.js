﻿BASE.require([
		"jQuery",
        "Date.prototype.format"
], function () {
    var Future = BASE.async.Future;
    BASE.namespace("app.components.inputs");

    var defaultValidateAsync = function () {
        return Future.fromResult();
    };
    var defaultMap = function (value) {
        return value;
    };

    app.components.inputs.Text = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $label = $(tags["label"]);
        var $input = $(tags["input"]);
        var $error = $(tags["error-message"]);
        var config = null;
        var map = defaultMap;
        var validateAsync = defaultValidateAsync;
        var value = null;
        var property = null;
        var Type = null;
        var entity = null;
        var displayService = null;
        var propertyName = null;

        var setValue = function (value) {
            $input.val(value);
        };

        var setLabel = function (label) {
            $label.text(label);
        };

        self.focus = function () {
            return $input.select();
        };

        self.getValue = function () {
            return map($input.val());
        };

        self.setConfig = function (config) {
            entity = config.entity;
            propertyName = config.propertyName;
            Type = config.Type;
            displayService = config.displayService;
            property = displayService.getDisplayByType(Type).properties[propertyName];
            value = entity[config.propertyName];

            setValue(value);
            setLabel(property.label());
        };

        self.saveAsync = function () {
            entity[propertyName] = self.getValue();
        };

        self.validateAsync = function () {
            return validateAsync(self.getValue()).catch(function (error) {
                $error.text(error.message);
            });
        };
    };
});