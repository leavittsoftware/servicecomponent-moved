﻿BASE.require([
		"jQuery",
        "Date.prototype.format",
        "Array.prototype.asQueryable"
], function () {
    var Future = BASE.async.Future;
    BASE.namespace("app.components.inputs");

    var defaultValidateAsync = function () {
        return Future.fromResult();
    };
    var defaultMap = function (value) {
        return value;
    };

    app.components.inputs.OneToOneTargetInput = function (elem, tags, services) {
        var self = this;
        var $elem = $(elem);
        var $label = $(tags["label"]);
        var $results = $(tags["results"]);
        var $selectButton = $(tags["select-button"]);
        var $description = $(tags["description"]);

        var property = null;
        var map = defaultMap;
        var validateAsync = defaultValidateAsync;
        var entity = null;
        var Type = null;
        var propertyName = null;
        var displayService = null;
        var primaryKeyPropertyName = null;
        var relationship = null;
        var value = null;
        var property = null;
        var oneToOneTargetFormFuture = null;

        var getOneToOneTargetModal = function () {
            if (oneToOneTargetFormFuture == null) {
                return entityFormFuture = services.get("windowService").createModalAsync({
                    componentName: "app/components/database/OneToOneTargetForm.html",
                    height: 500,
                    width: 700
                })
            }

            return oneToOneTargetFormFuture;
        };

        var setLabel = function (label) {
            $label.text(label);
        };

        var setDescription = function (value) {
            if (value == null) {
                $description.text("(None)");
                $selectButton.text("Select");
            } else {
                $description.text();
                $selectButton.text("Change");
            }
        };

        var getRelationship = function () { };

        self.setConfig = function (value) {
            entity = config.entity;
            Type = entity.constructor;
            propertyName = config.propertyName;
            displayService = config.displayService;
            property = displayService.getDisplayByType(Type).properties[propertyName];
            primaryKeyPropertyName = displayService.service.getEdm().getPrimaryKeyProperties(Type)[0];
            relationship = displayService.getRelationshipsByType(Type)[config.propertyName];
            value = entity[config.propertyName];

            setLabel(property.label());
            setDescription(value);
        };

        self.validateAsync = function () {
            return validateAsync(self.getValue()).catch(function (error) {
                $error.text(error.message);
            });
        };

        $manageButton.on("click", function () {
            getOneToManyCollectionModal().chain(function (windowManager) {
                var controller = windowManager.controller;
                var element = $(windowManager.element).addClass("absolute-fill-parent");
                var display = property.inputComponent.display;
                var relationship = property.inputComponent.relationship;

                controller.setConfigAsync({
                    relationship: relationship,
                    display: display,
                    entity: entity,
                    queryable: buildQueryable(display.service, relationship, entity)
                });

                return windowManager.window.showAsync();

            }).try();
        });
    };
});