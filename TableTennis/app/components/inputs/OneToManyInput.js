﻿BASE.require([
		"jQuery",
        "Date.prototype.format",
        "Array.prototype.asQueryable"
], function () {
    var Future = BASE.async.Future;
    BASE.namespace("app.components.inputs");

    var defaultValidateAsync = function () {
        return Future.fromResult();
    };
    var defaultMap = function (value) {
        return value;
    };

    app.components.inputs.OneToManyInput = function (elem, tags, services) {
        var self = this;
        var $elem = $(elem);
        var $label = $(tags["label"]);
        var $results = $(tags["results"]);
        var $manageButton = $(tags["manage-button"]);
        var map = defaultMap;
        var validateAsync = defaultValidateAsync;
        var value = null;
        var property = null;
        var Type = null;
        var parentEntity = null;
        var array = null;
        var displayService = null;
        var primaryKeyPropertyName = "id";
        var relationship = null;
        var propertyName = null;
        var oneToManyCollectionFuture = null;

        var buildQueryable = function (service, relationship, parentEntity) {
            if (parentEntity[primaryKeyPropertyName] == null) {
                return value.asQueryable();
            } else {
                return service.asQueryable(relationship.ofType).where(function (expBuilder) {
                    return expBuilder.property(relationship.withForeignKey).isEqualTo(parentEntity[relationship.hasKey]);
                });
            }
        };

        var getOneToManyCollectionModal = function () {
            if (oneToManyCollectionFuture == null) {
                return oneToManyCollectionFuture = services.get("windowService").createModalAsync({
                    componentName: "app/components/database/OneToManyCollection.html",
                    height: 500,
                    width: 700
                })
            }

            return oneToManyCollectionFuture;
        };

        var setLabel = function (label) {
            $label.text(label);
        };

        self.setConfig = function (config) {
            parentEntity = config.entity;
            Type = parentEntity.constructor;
            propertyName = config.propertyName;
            displayService = config.displayService;
            property = displayService.getDisplayByType(Type).properties[propertyName];
            primaryKeyPropertyName = displayService.service.getEdm().getPrimaryKeyProperties(Type)[0];
            relationship = displayService.getRelationshipsByType(Type)[config.propertyName];
            array = parentEntity[config.propertyName];

            setLabel(property.label());
        };

        self.saveAsync = function () {
            return Future.fromResult(array);
        };

        self.validateAsync = function () {
            return validateAsync(parentEntity[propertyName]).catch(function (error) {
                $error.text(error.message);
            });
        };

        $manageButton.on("click", function () {
            getOneToManyCollectionModal().chain(function (windowManager) {
                var controller = windowManager.controller;
                var element = $(windowManager.element).addClass("absolute-fill-parent");

                controller.setConfigAsync({
                    entity: parentEntity,
                    displayService: displayService,
                    relationship: relationship,
                    propertyName: propertyName
                }).try();

                return windowManager.window.showAsync();

            }).try();
        });
    };
});