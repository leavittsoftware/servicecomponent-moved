﻿BASE.require([
		"jQuery",
        "BASE.collections.Hashmap"
], function () {
    var Hashmap = BASE.collections.Hashmap;

    BASE.namespace("app.components.database");

    app.components.database.Table = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $header = $(tags["header"]);
        var list = $(tags["list"]).controller();
        var layout = list.getLayout();
        var selectedItems = new Hashmap();
        var queryable = null;
        var delegate = null;
        var columns = null;
        var listItemConfig = null;
        var primaryKeyProperty = "id";

        var createHeader = function (column) {
            var name = column.name;
            var label = column.label;
            var left = column.left;
            var width = column.width;

            var $header = $("<button></button>");

            $header.text(label).addClass("xp-button");
            $header.css({
                boxSizing: "border-box",
                height: "25px",
                fontSize: "14px",
                lineHeight: "25px",
                width: width + "%",
                paddingLeft: "10px",
                textAlign: "left",
                position: "absolute",
                top: "0",
                left: left + "%"
            });

            $header.on("click", function () {
                var orderBy = $header.attr("order-by");
                if (orderBy === "asc") {
                    $header.attr("order-by", "desc");
                    list.setQueryableAsync(queryable.orderByDesc(function (expBuilder) {
                        return expBuilder.property(name);
                    })).try();
                } else {
                    $header.attr("order-by", "asc");
                    list.setQueryableAsync(queryable.orderBy(function (expBuilder) {
                        return expBuilder.property(name);
                    })).try();
                }
            });

            return $header;
        };

        var createHeaders = function (columns) {
            return columns.reduce(function (columns, column) {
                var $column = createHeader(column);
                columns[column.name] = $column;

                $header.append($column);

                return columns;
            }, {});
        };

        var createColumns = function (properties) {
            var remainingWidth = 100;
            var width = 100 / properties.length;

            return properties.map(function (propertyName) {
                var column = {
                    name: propertyName,
                    label: delegate.getPropertyLabel(propertyName),
                    width: width,
                    left: 100 - remainingWidth
                };

                remainingWidth -= width;
                return column;
            });
        };

        self.setDelegate = function (value) {
            var properties;
            var headers;

            delegate = value;
            properties = delegate.getPropertyNames();
            columns = createColumns(properties);
            headers = createHeaders(columns);
            primaryKeyProperty = delegate.getPrimaryKeyPropertyName();
            listItemConfig = {
                columns: columns,
                delegate: delegate
            };
            self.setQueryableAsync(delegate.search("")).try();
        };

        self.setQueryableAsync = function (value) {
            queryable = value;
            selectedItems.clear();
            return list.setQueryableAsync(queryable);
        };

        self.redrawItems = function () {
            return list.redrawItems();
        };

        self.getSelectedItems = function () {
            return selectedItems;
        };

        layout.prepareElement = function (element, item, index) {
            var controller = $(element).controller();

            controller.setConfig(listItemConfig);
            controller.setEntity(item, index);

            if (selectedItems.hasKey(item[primaryKeyProperty])) {
                controller.select();
            } else {
                controller.deselect();
            }
        };

        $elem.on("itemSelected", function (event) {
            selectedItems.add(event.entity[primaryKeyProperty], event.entity);

            $elem.trigger({
                type: "selectionChange",
                selectedItems: selectedItems
            });

            return false;
        });

        $elem.on("itemDeselected", function (event) {
            selectedItems.remove(event.entity[primaryKeyProperty]);

            $elem.trigger({
                type: "selectionChange",
                selectedItems: selectedItems
            });

            return false;
        });

    };
});