﻿BASE.require([
		"jQuery"
], function () {
    var Future = BASE.async.Future;
    var Fulfillment = BASE.async.Fulfillment;

    BASE.namespace("app.components.database");

    app.components.database.OneToOneTargetForm = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $cancel = $(tags["cancel"]);
        var $table = $(tags["table"])
        var table = $table.controller();
        var window = null;
        var fulfillment = null;

        self.setConfigAsync = function (config) {
            var displayService = config.displayService;
            var typeDisplay = displayService.getDisplayByType(config.relationship.type);
            fulfillment = new Fulfillment();
            var delegate = {
                search: function (text, orderBy) {
                    return typeDisplay.search(text, orderBy);
                },
                getPropertyLabel: function (propertyName) {
                    return typeDisplay.properties[propertyName].label();
                },
                getPropertyDisplay: function (entity, propertyName) {
                    return typeDisplay.properties[propertyName].display(entity[propertyName]);
                },
                getPropertyNames: function () {
                    return Object.keys(typeDisplay.properties);
                },
                getPrimaryKeyPropertyName: function () {
                    return displayService.edm.getPrimaryKeyProperties(Type)[0];
                }
            };
            table.setDelegate(delegate);
            return fulfillment;
        };

        self.init = function (windowManager) {
            window = windowManager;
        };

        $cancel.on("click", function () {
            fulfillment.cancel();
            window.closeAsync().try();
        });

        $table.on("selectionChange", function () {
            var entity = table.getSelectedItems().getValues()[0];
            fulfillment.setValue(entity);
            window.closeAsync().try();
            return false;
        });

    };
});