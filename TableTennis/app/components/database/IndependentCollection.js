﻿BASE.require([
		"jQuery",
        "BASE.data.DataContext"
], function () {
    var Future = BASE.async.Future;
    var DataContext = BASE.data.DataContext;

    BASE.namespace("app.components.database");

    app.components.database.IndependentCollection = function (elem, tags, services) {
        var self = this;
        var $elem = $(elem);
        var collectionForm = $(tags["collection"]).controller();
        var confirmDeleteModalFuture = null;
        var entityFormFuture = null;

        var getConfirmDeleteModal = function () {
            if (confirmDeleteModalFuture == null) {
                return confirmDeleteModalFuture = services.get("windowService").createModalAsync({
                    componentName: "app/components/Confirm.html",
                    height: 150,
                    width: 350
                })
            }

            return confirmDeleteModalFuture;
        };

        var getEntityFormModal = function () {
            if (entityFormFuture == null) {
                return entityFormFuture = services.get("windowService").createModalAsync({
                    componentName: "app/components/database/IndependentEntityForm.html",
                    height: 500,
                    width: 400
                })
            }

            return entityFormFuture;
        };

        self.setDisplay = function (Type, displayService) {
            var typeDisplay = displayService.getDisplayByType(Type);
            var delegate = {
                addAsync: function () {
                    var entity = new Type();
                    var window = null;
                    var dataContext = new DataContext(displayService.service);
                    dataContext.addEntity(entity);

                    return getEntityFormModal().chain(function (windowManager) {
                        window = windowManager.window;

                        window.setName("Add " + typeDisplay.labelInstance());

                        var controller = windowManager.controller;
                        var saveFuture = controller.setConfigAsync({
                            displayService: displayService,
                            entity: entity
                        });

                        windowManager.window.showAsync().try();
                        return saveFuture
                    }).chain(function () {
                        return dataContext.saveChangesAsync();
                    }).chain(function () {
                        return window.closeAsync();
                    }).finally(function () {
                        dataContext.dispose();
                    });
                },
                editAsync: function (entity) {
                    var window = null;
                    var dataContext = new DataContext(displayService.service);
                    dataContext.loadEntity(entity);

                    return getEntityFormModal().chain(function (windowManager) {
                        window = windowManager.window;

                        window.setName("Edit " + typeDisplay.labelInstance());

                        var controller = windowManager.controller;
                        var saveFuture = controller.setConfigAsync({
                            displayService: displayService,
                            entity: entity
                        });

                        windowManager.window.showAsync().try();
                        return saveFuture
                    }).chain(function () {
                        return dataContext.saveChangesAsync();
                    }).chain(function () {
                        return window.closeAsync();
                    }).finally(function () {
                        dataContext.dispose();
                    });
                },
                removeAsync: function (items) {
                    return getConfirmDeleteModal().chain(function (windowManager) {
                        var controller = windowManager.controller;
                        var confirmFuture = controller.getConfirmationForMessageAsync("Are you sure you want to delete these items?");

                        windowManager.window.showAsync().try();

                        return confirmFuture;
                    }).chain(function () {
                        var removeItemFutures = items.map(function (item) {
                            return displayService.service.remove(Type, item);
                        });
                        return Future.all(removeItemFutures);
                    });
                },
                search: function (text, orderBy) {
                    return typeDisplay.search(text, orderBy);
                },
                getPropertyLabel: function (propertyName) {
                    return typeDisplay.properties[propertyName].label();
                },
                getPropertyDisplay: function (entity, propertyName) {
                    return typeDisplay.properties[propertyName].display(entity[propertyName]);
                },
                getPropertyNames: function () {
                    return Object.keys(typeDisplay.properties).filter(function (name) {
                        var property = typeDisplay.properties[name];
                        return property.showOnList || typeof property.showOnList === "undefined";
                    });
                },
                getPrimaryKeyPropertyName: function () {
                    return displayService.edm.getPrimaryKeyProperties(Type)[0];
                }
            };

            collectionForm.setDelegate(delegate);
        };
    };
});