﻿BASE.require([
		"jQuery"
], function () {
    var Future = BASE.async.Future;
    var Fulfillment = BASE.async.Fulfillment;

    BASE.namespace("app.components.database");

    app.components.database.OneToManyCollectionEntityForm = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var form = $(tags["form"]).controller();
        var $cancel = $(tags["cancel"]);
        var $save = $(tags["save"]);
        var fulfillment = null;
        var config = null;
        var relationship = null;
        var displayService = null;
        var entityDisplay = null;
        var entity = null;
        var parentEntity = null;
        var propertyName = null;
        var properties = null;
        var primaryKeyPropertyName = null;
        var window = null;

        var saveAsync = function () {
            return form.validateAsync().chain(function () {
                return form.saveAsync();
            }).chain(function () {
                fulfillment.setValue(config.entity);
            });
        };

        self.setConfigAsync = function (value) {
            fulfillment = new Fulfillment();

            config = value;
            relationship = config.relationship;
            displayService = config.displayService;
            propertyName = relationship.withOne;
            parentEntity = config.parentEntity;
            entity = config.entity;
            entityDisplay = displayService.getDisplayByType(relationship.ofType);

            properties = Object.keys(entityDisplay.properties).reduce(function (properties, propertyName) {
                if (propertyName === relationship.withOne) {
                    return properties;
                }

                var property = entityDisplay.properties[propertyName];
                properties[propertyName] = property;

                return properties;
            }, {});



            form.setConfigAsync({
                properties: properties,
                displayService: displayService,
                entity: entity,
                Type: relationship.ofType
            }).try();


            return fulfillment;
        };

        self.init = function (windowManager) {
            window = windowManager;
        };

        self.deactivated = function () {
            fulfillment.cancel();
        };

        $cancel.on("click", function () {
            window.closeAsync().try();
            fulfillment.cancel();
        });

        $save.on("click", function () {
            saveAsync().try();
        });

        $elem.on("keydown", function (event) {
            if (event.which === 13) {
                saveAsync().try();
            }
        });
    };
});