﻿BASE.require([
		"jQuery",
], function () {
    BASE.namespace("app.components.database");
    var Future = BASE.async.Future;

    app.components.database.Form = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var currentComponents = null;
        var componentsByPropertyName = null;
        var displayService = null;
        var entity = null;
        var Type = null;

        var generateFormAsync = function (properties) {
            componentsByPropertyName = {};
            $elem.empty();

            var columnFutures = Object.keys(properties).filter(function (name) {
                var property = properties[name];
                return typeof property.inputComponent === "object" && property.inputComponent != null;
            }).map(function (name) {
                var property = properties[name];
                var inputComponent = property.inputComponent.name;

                return BASE.web.components.createComponentAsync(inputComponent).chain(function (element) {
                    return {
                        name: name,
                        element: element,
                        property: property
                    };
                });
            });

            return Future.all(columnFutures).chain(function (columns) {
                currentComponents = columns.map(function (column) {
                    var $input = $(column.element).css({
                        width: "100%"
                    });

                    var inputController = $(column.element).controller();

                    inputController.setConfig({
                        entity: entity,
                        displayService: displayService,
                        propertyName: column.name,
                        Type: Type
                    });

                    $input.appendTo($elem);
                    componentsByPropertyName[column.name] = inputController;

                    return {
                        column: column,
                        inputController: inputController
                    };
                });
            });
        };

        self.setConfigAsync = function (config) {
            entity = config.entity;
            Type = config.Type;
            displayService = config.displayService;

            return generateFormAsync(config.properties).chain(function () {
                if (currentComponents.length > 0) {
                    setTimeout(function () {
                        currentComponents[0].inputController.focus();
                    }, 0);
                }
            }).try();
        };

        self.validateAsync = function () {
            var validationFutures = currentComponents.map(function (columnData) {
                columnData.inputController.validateAsync();
            });

            return Future.all(validationFutures);
        };

        self.saveAsync = function () {
            var saveFutures = currentComponents.map(function (columnData) {
                columnData.inputController.saveAsync();
            });

            return Future.all(saveFutures);
        };

        self.getValueByPropertyName = function (name) {
            return componentsByPropertyName[name].getValue();
        };

    };
});