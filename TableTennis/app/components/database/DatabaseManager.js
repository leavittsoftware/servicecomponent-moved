﻿BASE.require([
		"jQuery"
], function () {


    BASE.namespace("app.components.database");

    app.components.database.DatabaseManager = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $container = $(tags["table-container"]);
        var $tableNameList = $(tags["table-names-list"]);
        var tableNameList = $tableNameList.controller();
        var selectedCollectionName = null;
        var layout = tableNameList.getLayout();
        var mapping = null;
        var modelNameToModelType = null;
        var stateManager = null;
        var states = null;

        layout.prepareElement = function (element, item, index) {
            var controller = $(element).controller();
            controller.setDisplay(item);

            if (item.labelList() === selectedCollectionName) {
                controller.select();
            } else {
                controller.deselect();
            }
        };

        var createIndependentCollection = function (name) {
            var $div = $("<div></div>");
            $div.attr("component", "app/components/database/IndependentCollection.html");
            $div.attr("name", name);
            $div.addClass("absolute-fill-parent");
            return $div;
        };

        var createIndependentCollectionsAsync = function (displayService) {
            $container.empty();

            var fragment = document.createDocumentFragment();
            var service = displayService.service;
            var edm = service.getEdm();
            var models = edm.getModels();
            var modalNames = models.getKeys();
            mapping = {};
            modelNameToModelType = {};

            var applicableModels = modalNames.filter(function (modelName) {
                var model = models.get(modelName);

                var entity = new model.type();

                var isOptionalOneToOne = edm.getOneToOneAsTargetRelationships(entity).every(function (relationship) {
                    return relationship.optional;
                });

                var isOptionslOneToMany = edm.getOneToManyAsTargetRelationships(entity).every(function (relationship) {
                    return relationship.optional;
                });

                return isOptionalOneToOne && isOptionslOneToMany;

            });

            var collectionDisplays = applicableModels.map(function (modelName) {
                var model = models.get(modelName);
                var collectionDisplay = displayService.getDisplayByType(model.type);

                var label = collectionDisplay.labelList();
                createIndependentCollection(label).appendTo(fragment);
                mapping[label] = collectionDisplay;
                modelNameToModelType[label] = model;

                return collectionDisplay;
            });


            return BASE.web.components.createComponentAsync("state-manager", fragment).chain(function (element) {
                var $stateManager = $(element);
                $stateManager.addClass("absolute-fill-parent");
                stateManager = $stateManager.controller();
                states = stateManager.getStates();

                Object.keys(states).forEach(function (stateName) {
                    var collectionController = $(states[stateName]).controller();
                    var collectionManager = mapping[stateName];
                    var model = modelNameToModelType[stateName];

                    collectionController.setDisplay(model.type, displayService)
                });

                $container.append(element);
                tableNameList.setQueryableAsync(collectionDisplays.asQueryable()).try();
                self.selectCollectionByNameAsync(Object.keys(states)[0]).try();
            });
        };

        self.setDisplayServiceAsync = function (value) {
            return createIndependentCollectionsAsync(value).chain(function () {
                displayService = value;
            });
        };

        self.selectCollectionByNameAsync = function (name) {
            selectedCollectionName = name;
            return stateManager.pushAsync(name).chain(function () {
                return tableNameList.redrawItems();
            });
        };

        $elem.on("collectionSelect", function (event) {
            self.selectCollectionByNameAsync(event.collectionName).try();
        });
    };
});