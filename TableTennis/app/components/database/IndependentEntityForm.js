﻿BASE.require([
		"jQuery"
], function () {
    var Future = BASE.async.Future;
    var Fulfillment = BASE.async.Fulfillment;

    BASE.namespace("app.components.database");

    app.components.database.IndependentEntityForm = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var form = $(tags["form"]).controller();
        var $cancel = $(tags["cancel"]);
        var $save = $(tags["save"]);
        var displayService = null;
        var entityDisplay = null;
        var entity = null;
        var fulfillment = null;
        var window = null;

        var saveAsync = function () {
            return form.validateAsync().chain(function () {
                return form.saveAsync();
            }).chain(function () {
                fulfillment.setValue(config.entity);
            });
        };

        self.setConfigAsync = function (value) {
            config = value;
            fulfillment = new Fulfillment();

            displayService = config.displayService;
            entity = config.entity;
            entityDisplay = displayService.getDisplayByType(entity.constructor);

            form.setConfigAsync({
                displayService: displayService,
                entity: entity,
                properties: entityDisplay.properties,
                Type: entity.constructor
            }).try();

            return fulfillment;
        };

        self.init = function (windowManager) {
            window = windowManager;
        };

        self.deactivated = function () {
            fulfillment.cancel();
        };

        $cancel.on("click", function () {
            window.closeAsync().try();
            fulfillment.cancel();
        });

        $save.on("click", function () {
            saveAsync().try();
        });

        $elem.on("keydown", function (event) {
            if (event.which === 13) {
                saveAsync().try();
            }
        });
    };
});