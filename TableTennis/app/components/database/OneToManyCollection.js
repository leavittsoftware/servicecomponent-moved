﻿BASE.require([
		"jQuery",
        "BASE.data.DataContext",
        "BASE.data.utils"
], function () {
    var Future = BASE.async.Future;
    var Fulfillment = BASE.async.Fulfillment;
    var DataContext = BASE.data.DataContext;
    var emptyFuture = Future.fromResult();
    var cloneEntity = BASE.data.utils.shallowCloneEntity;

    BASE.namespace("app.components.database");

    app.components.database.OneToManyCollection = function (elem, tags, services) {
        var self = this;
        var $elem = $(elem);
        var $ok = $(tags["ok"]);
        var collectionForm = $(tags["collection"]).controller();
        var confirmDeleteModalFuture = null;
        var entityFormFuture = null;
        var fulfillment = null;
        var parentEntity = null;
        var relationship = null;
        var displayService = null;
        var window = null;
        var array = null;

        var getConfirmDeleteModal = function () {
            if (confirmDeleteModalFuture == null) {
                return confirmDeleteModalFuture = services.get("windowService").createModalAsync({
                    componentName: "app/components/Confirm.html",
                    height: 150,
                    width: 350
                })
            }

            return confirmDeleteModalFuture;
        };

        var getEntityFormModal = function () {
            if (entityFormFuture == null) {
                return entityFormFuture = services.get("windowService").createModalAsync({
                    componentName: "app/components/database/OneToManyCollectionEntityForm.html",
                    height: 500,
                    width: 400
                })
            }

            return entityFormFuture;
        };

        var setWindowName = function (displayService, relationship, entity) {
            var sourceDisplay = displayService.getDisplayByType(relationship.type);

            var name = "Managing " + sourceDisplay.properties[relationship.hasMany].label() + " of '" + sourceDisplay.displayInstance(entity) + "'";
            window.setName(name);
        };

        self.prepareToDeactivateAsync = function () {
            var future = emptyFuture;

            if (parentEntity[relationship.hasKey]) {
                var dataContext = new DataContext(displayService.service);
                dataContext.loadEntity(parentEntity);
                future = dataContext.saveChangesAsync().chain(function () {
                    dataContext.dispose();
                });
            }

            return future.chain(function () {
                fulfillment.setValue(parentEntity[relationship.hasMany]);
            });
        };

        self.setConfigAsync = function (config) {
            fulfillment = new Fulfillment();
            displayService = config.displayService;
            parentEntity = config.entity;
            relationship = config.relationship;

            var Type = relationship.ofType;
            var typeDisplay = displayService.getDisplayByType(Type);
            var queryable;
            var delegate;

            setWindowName(displayService, relationship, parentEntity);

            if (parentEntity[relationship.hasKey] == null) {
                array = [];
                queryable = array.asQueryable();

                delegate = {
                    addAsync: function () {
                        var entity = new Type();

                        var window = null;
                        return getEntityFormModal().chain(function (windowManager) {
                            window = windowManager.window;

                            window.setName("Add " + typeDisplay.labelInstance());

                            var controller = windowManager.controller;
                            var saveFuture = controller.setConfigAsync({
                                displayService: displayService,
                                parentEntity: parentEntity,
                                entity: entity,
                                relationship: relationship
                            });

                            windowManager.window.showAsync().try();
                            return saveFuture
                        }).chain(function () {
                            array.push(entity);
                            parentEntity[relationship.hasMany].push(entity);
                            return window.closeAsync();
                        });
                    },
                    editAsync: function (entity) {
                        var window = null;
                        return getEntityFormModal().chain(function (windowManager) {
                            window = windowManager.window;

                            window.setName("Edit " + typeDisplay.labelInstance());

                            var controller = windowManager.controller;
                            var saveFuture = controller.setConfigAsync({
                                displayService: displayService,
                                parentEntity: parentEntity,
                                entity: entity,
                                relationship: relationship
                            });

                            windowManager.window.showAsync().try();
                            return saveFuture
                        }).chain(function () {
                            return window.closeAsync();
                        });
                    },
                    removeAsync: function (items) {
                        return getConfirmDeleteModal().chain(function (windowManager) {
                            var controller = windowManager.controller;
                            var confirmFuture = controller.getConfirmationForMessageAsync("Are you sure you want to delete these items?");

                            windowManager.window.showAsync().try();

                            return confirmFuture;
                        }).chain(function () {
                            items.forEach(function (item) {
                                var index = parentEntity[relationship.hasMany].indexOf(item);

                                if (index > -1) {
                                    parentEntity[relationship.hasMany].splice(index, 1);
                                }

                                index = array.indexOf(item);

                                if (index > -1) {
                                    array.splice(index, 1);
                                }
                            });
                        });
                    },
                    search: function (text, orderBy) {
                        var searchQueryable = typeDisplay.search(text, orderBy);
                        return queryable.merge(searchQueryable);
                    },
                    getPropertyLabel: function (propertyName) {
                        return typeDisplay.properties[propertyName].label();
                    },
                    getPropertyDisplay: function (entity, propertyName) {
                        return typeDisplay.properties[propertyName].display(entity[propertyName]);
                    },
                    getPropertyNames: function () {
                        var propertyNames = Object.keys(typeDisplay.properties).filter(function (name) {
                            var property = typeDisplay.properties[name];
                            return name !== relationship.withOne && (property.showOnList || typeof property.showOnList === "undefined");
                        });
                        return propertyNames;
                    },
                    getPrimaryKeyPropertyName: function () {
                        return displayService.edm.getPrimaryKeyProperties(Type)[0];
                    }
                };


            } else {
                queryable = displayService.service.asQueryable(relationship.ofType).where(function (expBuilder) {
                    return expBuilder.property(relationship.withForeignKey).isEqualTo(parentEntity[relationship.hasKey]);
                });

                delegate = {
                    addAsync: function () {
                        var entity = new Type();

                        var window = null;
                        return getEntityFormModal().chain(function (windowManager) {
                            window = windowManager.window;

                            window.setName("Add " + typeDisplay.labelInstance());

                            var controller = windowManager.controller;
                            var saveFuture = controller.setConfigAsync({
                                displayService: displayService,
                                parentEntity: parentEntity,
                                entity: entity,
                                relationship: relationship
                            });

                            windowManager.window.showAsync().try();
                            return saveFuture
                        }).chain(function (entity) {
                            var dataContext = new DataContext(displayService.service);
                            entity = dataContext.loadEntity(entity);
                            entity[relationship.withForeignKey] = parentEntity[relationship.hasKey];
                            return dataContext.saveChangesAsync().chain(function () {
                                return dataContext.dispose();
                            });
                        }).chain(function () {
                            return window.closeAsync();
                        });
                    },
                    editAsync: function (entity) {
                        var window = null;

                        var firstEntity = entity;

                        var dataContext = new DataContext(displayService.service);
                        entity = dataContext.loadEntity(entity);

                        return getEntityFormModal().chain(function (windowManager) {
                            window = windowManager.window;

                            window.setName("Edit " + typeDisplay.labelInstance());

                            var controller = windowManager.controller;
                            var saveFuture = controller.setConfigAsync({
                                displayService: displayService,
                                parentEntity: parentEntity,
                                entity: entity,
                                relationship: relationship
                            });

                            windowManager.window.showAsync().try();
                            return saveFuture
                        }).chain(function (entity) {
                            return dataContext.saveChangesAsync().chain(function () {
                                return dataContext.dispose();
                            });
                        }).chain(function () {
                            collectionForm.searchAsync("").try();
                            return window.closeAsync();
                        });
                    },
                    removeAsync: function (items) {
                        return getConfirmDeleteModal().chain(function (windowManager) {
                            var controller = windowManager.controller;
                            var confirmFuture = controller.getConfirmationForMessageAsync("Are you sure you want to delete these items?");

                            windowManager.window.showAsync().try();

                            return confirmFuture;
                        }).chain(function () {
                            var removeItemFutures = items.map(function (item) {
                                return displayService.service.remove(Type, item);
                            });
                            return Future.all(removeItemFutures);
                        });
                    },
                    search: function (text, orderBy) {
                        var searchQueryable = typeDisplay.search(text, orderBy);
                        return queryable.merge(searchQueryable);
                    },
                    getPropertyLabel: function (propertyName) {
                        return typeDisplay.properties[propertyName].label();
                    },
                    getPropertyDisplay: function (entity, propertyName) {
                        return typeDisplay.properties[propertyName].display(entity[propertyName]);
                    },
                    getPropertyNames: function () {
                        var propertyNames = Object.keys(typeDisplay.properties).filter(function (name) {
                            var property = typeDisplay.properties[name];
                            return name !== relationship.withOne && (typeof property.showOnList  === "undefined" || property.showOnList);
                        });
                        return propertyNames;
                    },
                    getPrimaryKeyPropertyName: function () {
                        return displayService.edm.getPrimaryKeyProperties(Type)[0];
                    }
                };
            }

            collectionForm.setDelegate(delegate);
            return fulfillment;
        };

        self.init = function (windowManager) {
            window = windowManager;
        };

        $ok.on("click", function () {
            window.closeAsync().try();
        });
    };
});