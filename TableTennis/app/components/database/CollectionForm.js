﻿BASE.require([
		"jQuery",
        "String.prototype.trim"
], function () {
    var Future = BASE.async.Future;

    var delegateInterface = [
        "addAsync",
        "editAsync",
        "removeAsync",
        "search",
        "getPropertyLabel",
        "getPropertyDisplay",
        "getPropertyNames",
        "getPrimaryKeyPropertyName"
    ];

    var implementsInterface = function (methodNames, obj) {
        return methodNames.every(function (methodName) {
            return typeof obj[methodName] === "function";
        });
    };

    BASE.namespace("app.components.database");

    app.components.database.CollectionForm = function (elem, tags, services) {
        var self = this;
        var $elem = $(elem);
        var $search = $(tags["search"]);
        var $edit = $(tags["edit"]);
        var $add = $(tags["add"]);
        var $delete = $(tags["delete"]);
        var table = $(tags["table"]).controller();
        var delegate = null;
        var orderBy = null;

        var search = function () {
            var search = $search.val();
            return table.setQueryableAsync(delegate.search(search, orderBy)).try();
        };

        var handleActionButtons = function (selectedItems) {
            var selectedAmount = selectedItems.getKeys().length;
            if (selectedAmount === 0) {
                $edit.attr("disabled", "disabled");
                $delete.attr("disabled", "disabled");
            } else if (selectedAmount > 0) {
                $edit.removeAttr("disabled");
                $delete.removeAttr("disabled");
            }

            if (selectedAmount > 1) {
                $edit.attr("disabled", "disabled");
            }
        };

        var editItemAsync = function (item) {
            return delegate.editAsync(item).chain(function () {
                var selectedItems = table.getSelectedItems();
                selectedItems.clear();
                handleActionButtons(selectedItems);
                return table.redrawItems();
            });
        };

        self.setDelegate = function (value) {
            var isValidDelegate = implementsInterface(delegateInterface, value);

            if (!isValidDelegate) {
                throw new Error("Invalid delegate it needs to implement all these methods." + delegateInterface.join(", "));
            }

            delegate = value;
            table.setDelegate(delegate);
        };

        self.searchAsync = function (text, orderBy) {
            return table.setQueryableAsync(delegate.search(text, orderBy));
        };

        $elem.on("selectionChange", function (event) {
            var selectedItems = event.selectedItems;
            handleActionButtons(selectedItems);

            return false;
        });

        $edit.on("click", function () {
            editItemAsync(table.getSelectedItems().getValues()[0]).try();
        });

        $delete.on("click", function () {
            delegate.removeAsync(table.getSelectedItems().getValues()).chain(function () {
                var selectedItems = table.getSelectedItems();
                selectedItems.clear();
                handleActionButtons(selectedItems);
                return table.setQueryableAsync(delegate.search(""));
            }).try();
        });

        $add.on("click", function () {
            delegate.addAsync().chain(function () {
                var selectedItems = table.getSelectedItems();
                selectedItems.clear();
                handleActionButtons(selectedItems);
                return table.setQueryableAsync(delegate.search(""));
            }).try();
        });

        $elem.on("itemDoubleClicked", function (event) {
            editItemAsync(event.entity).try();
        });

        $search.on("keyup", search);

    };
});