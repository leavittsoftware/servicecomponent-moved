﻿BASE.require([
    "jQuery",
    "jQuery.fn.region",
    "BASE.util.invokeMethodIfExistsAsync",
    "BASE.util.invokeMethodIfExists"
], function () {
    var invokeMethodIfExistsAsync = BASE.util.invokeMethodIfExistsAsync;
    var invokeMethodIfExists = BASE.util.invokeMethodIfExists;
    var Future = BASE.async.Future;
    var fulfilledFuture = Future.fromResult();

    BASE.namespace("app.components");

    app.components.Window = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $container = $(tags["container"]);
        var $handle = $(tags["handle"]);
        var $close = $(tags["close"]);
        var $name = $(tags["name"]);
        var regionConstraint = null;
        var mouseStartXY = null;
        var windowStartXY = null;
        var delegate = {};

        self.getComponent = function () {
            return $container.children()[0];
        };

        self.setRegionContraint = function (region) {
            regionConstraint = region;
        };

        self.closeAsync = function () {
            if (delegate && typeof delegate.closeAsync === "function") {
                return delegate.closeAsync();
            }
            return fulfilledFuture;
        };

        self.showAsync = function () {
            if (delegate && typeof delegate.showAsync === "function") {
                return delegate.showAsync();
            }
            return fulfilledFuture;
        };

        self.disposeAsync = function () {
            if (delegate && typeof delegate.disposeAsync === "function") {
                return delegate.disposeAsync();
            }
            return fulfilledFuture;
        };

        self.setDelegate = function (value) {
            delegate = value;
        };

        self.setName = function (name) {
            $name.text(name);
        };

        var mousemove = function (event) {
            var cursorDeltaX = event.pageX - mouseStartXY.x;
            var cursorDeltaY = event.pageY - mouseStartXY.y;

            var coords = {
                left: cursorDeltaX + windowStartXY.x,
                top: cursorDeltaY + windowStartXY.y
            }

            $elem.offset(coords);
        };

        var mouseup = function (event) {
            $(document.body).off("mousemove", mousemove);
            $(document.body).off("mouseup", mouseup);
        };

        $handle.on("mousedown", function (event) {
            invokeMethodIfExists(delegate, "focus");

            mouseStartXY = {
                x: event.pageX,
                y: event.pageY
            }

            windowStartXY = $elem.region();

            $(document.body).on("mousemove", mousemove);
            $(document.body).on("mouseup", mouseup);
            $(document.body).on("mouseleave", mouseup);
            return false;
        });

        $close.on("mousedown", function () {
            invokeMethodIfExists(delegate, "focus");
            return false;
        });

        $close.on("click", function () {
            self.closeAsync().try();
        });

        $elem.on("mousedown", function () {
            invokeMethodIfExists(delegate, "focus");
        });
    };
});