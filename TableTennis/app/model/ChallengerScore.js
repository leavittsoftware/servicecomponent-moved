﻿BASE.namespace("app.model");

app.model.ChallengerScore = function () {

    this.id = null;

    this.gameId = null;
    this.game = null;

    this.playerId = null;
    this.player = null;

    this.value = null;

};