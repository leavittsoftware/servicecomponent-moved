﻿BASE.namespace("app.model");

app.model.Game = function () {

    this.id = null;
    this.date = null;
    this.matchId = null;
    this.match = null;
    this.defenderScore = null;
    this.challengerScore = null;

};