﻿BASE.namespace("app.model");

app.model.DefenderScore = function () {

    this.id = null;

    this.gameId = null;
    this.game = null;

    this.playerId = null;
    this.player = null;

    this.value = null;

};