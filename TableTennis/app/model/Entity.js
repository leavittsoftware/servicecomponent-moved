﻿BASE.namespace("app.model");

app.model.Entity = function () {
    this.id = null;
    this.name = null;
    this.namespace = null;
    this.properties = [];
};