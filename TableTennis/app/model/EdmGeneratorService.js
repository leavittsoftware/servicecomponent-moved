﻿BASE.require([
    "app.model.EdmGeneratorEdm",
    "BASE.data.databases.Sqlite",
    "BASE.data.services.SqliteService"
], function () {

    var Edm = app.model.EdmGeneratorEdm;
    var Sqlite = BASE.data.databases.Sqlite;
    var Service = BASE.data.services.SqliteService;

    BASE.namespace("app.model");

    app.model.EdmGeneratorService = function () {
        var edm = new Edm();

        var database = new Sqlite({
            name: "edm_generator",
            edm: edm
        });

        Service.call(this, database);
    };

    BASE.extend(app.model.Service, Service);

});