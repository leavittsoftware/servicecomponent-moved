﻿BASE.require([
    "BASE.data.Edm",
    "app.model.Entity",
    "app.model.Property",
    "app.model.PropertyAttribute",
    "app.model.ValueType"
], function () {

    var Future = BASE.async.Future;
    var Edm = BASE.data.Edm;
    var Entity = app.model.Entity;
    var Property = app.model.Property;
    var PropertyAttribute = app.model.PropertyAttribute;
    var ValueType = app.model.ValueType;

    BASE.namespace("app.model");

    app.model.EdmGeneratorEdm = function () {
        Edm.call(this);

        this.addModel({
            type: Entity,
            collectionName: "entities",
            properties: {
                id: {
                    type: Integer,
                    primaryKey: true,
                    autoIncrement: true,
                },
                name: {
                    type: String
                },
                namespace: {
                    type: String
                }
            }
        });

        this.addModel({
            type: Property,
            collectionName: "properties",
            properties: {
                id: {
                    type: Integer,
                    primaryKey: true,
                    autoIncrement: true,
                },
                name: {
                    type: String
                },
                entityId: {
                    type: Integer
                },
                valueTypeId: {
                    type: Integer
                },
                isNullable: {
                    type: Boolean
                },
                isPrimaryKey: {
                    type: Boolean
                },
                isAutoIncrement: {
                    type: Boolean
                }
            }
        });

        this.addModel({
            type: PropertyAttribute,
            collectionName: "propertyAttributes",
            properties: {
                id: {
                    type: Integer,
                    primaryKey: true,
                    autoIncrement: true,
                },
                key: {
                    type: String
                },
                value: {
                    type: String
                },
                propertyId: {
                    type: Integer
                }
            }
        });

        this.addModel({
            type: ValueType,
            collectionName: "valueTypes",
            properties: {
                id: {
                    type: Integer,
                    primaryKey: true,
                    autoIncrement: true,
                },
                name: {
                    type: String
                },
                namespace: {
                    type: String
                }
            }
        });


        this.addOneToMany({
            type: Entity,
            hasKey: "id",
            hasMany: "properties",
            ofType: Property,
            withKey: "id",
            withForeignKey: "entityId",
            withOne: "Entity"
        });

        this.addOneToMany({
            type: Property,
            hasKey: "id",
            hasMany: "attributes",
            ofType: PropertyAttribute,
            withKey: "id",
            withForeignKey: "propertyId",
            withOne: "property"
        });

        this.addOneToMany({
            type: ValueType,
            hasKey: "id",
            hasMany: "properties",
            ofType: Property,
            withKey: "id",
            withForeignKey: "valueTypeId",
            withOne: "valueType"
        });

    };


    BASE.extend(app.model.EdmGeneratorEdm, Edm);

});