﻿BASE.require([
    "BASE.data.Edm",
    "String.prototype.toEnum",
    "String.prototype.toEnumFlag",
    "Number.prototype.toEnumString",
    "Number.prototype.toEnumFlagString",
    "Date.prototype.format"
], function () {
    var prettifyRegex = /([A-Z]*?[A-Z]|^.)/g;

    var prettifyName = function (name) {
        return name.replace(prettifyRegex, function (match, part1, offset) {
            if (offset === 0) {
                return part1.toUpperCase();
            }
            return " " + part1.toUpperCase();
        });
    };

    var createStringInput = function (modelProperty, property) {

        if (typeof modelProperty.length === "number" && modelProperty.length > 255) {
            property.inputComponent = {
                name: "app/components/inputs/Textarea.html"
            };
        } else {
            property.inputComponent = {
                name: "app/components/inputs/Text.html"
            };
        }

        property.display = function (value) {
            if (value == null) {
                return "";
            }
            return value.toString();
        };
    };

    var createBooleanInput = function (property) {
        property.inputComponent = {
            name: "app/components/inputs/Select.html",
            options: [{
                label: "Yes",
                value: true
            }, {
                label: "No",
                value: false
            }]
        };

        property.display = function (value) {
            if (value == null) {
                return "Unknown";
            }
            return value ? "Yes" : "No";
        };
    };

    var createNumberInput = function (property) {
        property.inputComponent = {
            name: "app/components/inputs/Number.html"
        };

        property.display = function (value) {
            if (value == null) {
                return "";
            }
            return value.toString();
        };
    };

    var createDateInput = function (property) {
        property.inputComponent = {
            name: "app/components/inputs/Date.html"
        };

        property.display = function (value) {
            if (value == null) {
                return "";
            }

            return value.format("mm/dd/yyyy");
        };
    };

    var createEnumInput = function (modelProperty, property) {
        var Type = modelProperty.type;

        var options = Object.keys(Type).filter(function (key) {
            return Type[key] instanceof Enum;
        }).map(function (key) {
            var enumumeration = Type[key];
            return {
                label: enumumeration.displayName || enumumeration.name,
                value: enumumeration.name
            };
        });

        property.inputComponent = {
            name: "app/components/inputs/Select.html",
            options: options
        };
    };

    var createEnumFlagInput = function (modelProperty, property) {
        var Type = modelProperty.type;

        var options = Object.keys(Type).filter(function (key) {
            return Type[key] instanceof EnumFlag;
        }).map(function (key) {
            var enumumeration = Type[key];
            return {
                label: enumumeration.displayName || enumumeration.name,
                value: enumumeration.name
            };
        });

        property.input = {
            name: "app/components/inputs/Select.html",
            options: options
        };
    };

    BASE.namespace("app.model");

    var TypeDisplay = function (Type, service) {
        var edm = service.getEdm();
        var numberPropertyNames = [];
        var datePropertyNames = [];
        var stringPropertyNames = [];
        var booleanPropertyNames = [];
        var model = this.model = edm.getModelByType(Type);
        var relationships = this.createRelationshipProperties(edm, model);
        var keys = edm.getAllKeyProperties(Type).concat(edm.getAllKeyProperties(Type));
        this.type = model.type;

        this.search = function (text) {
            if (text === "") {
                return service.asQueryable(Type);
            }

            var tokens = text.split(" ");
            return service.asQueryable(Type).where(function (expBuilder) {
                var ands = [];

                tokens.forEach(function (token) {
                    var numberValue = parseInt(token);
                    var dateValue = new Date(token);
                    var stringValue = token;
                    var ors;

                    if (!isNaN(numberValue)) {
                        ors = numberPropertyNames.map(function (name) {
                            return expBuilder.property(name).isEqualTo(numberValue);
                        });
                    } else if (!isNaN(dateValue.getTime())) {
                        ors = datePropertyNames.map(function (name) {
                            return expBuilder.property(name).isEqualTo(dateValue);
                        });
                    } else {
                        ors = stringPropertyNames.map(function (name) {
                            return expBuilder.property(name).contains(stringValue);
                        });
                    }

                    if (ors.length > 0) {
                        ands.push(expBuilder.or.apply(expBuilder, ors));
                    }
                });

                return expBuilder.and.apply(expBuilder, ands);
            });

        };

        this.properties = Object.keys(model.properties).filter(function (propertyName) {
            return keys.indexOf(propertyName) === -1;
        }).reduce(function (accumulated, propertyName) {
            var modelProperty = model.properties[propertyName];

            var property = {
                label: function () {
                    return prettifyName(propertyName);
                },
                display: function (value) {
                    return value;
                },
                sortOrder: 0
            };

            if (relationships[propertyName]) {
                property.inputComponent = relationships[propertyName];
                property.showOnList = false;
            } else if (modelProperty.type === String) {
                stringPropertyNames.push(propertyName);
                createStringInput(modelProperty, property);
            } else if (modelProperty.type === Number) {
                numberPropertyNames.push(propertyName);
                createNumberInput(property);
            } else if (modelProperty.type === Boolean) {
                booleanPropertyNames.push(propertyName);
                createBooleanInput(property);
            } else if (modelProperty.type === Double) {
                numberPropertyNames.push(propertyName);
                createNumberInput(property);
            } else if (modelProperty.type === Float) {
                numberPropertyNames.push(propertyName);
                createNumberInput(property);
            } else if (modelProperty.type === Integer) {
                numberPropertyNames.push(propertyName);
                createNumberInput(property);
            } else if (modelProperty.type === Binary) {
                numberPropertyNames.push(propertyName);
                createNumberInput(property);
            } else if (modelProperty.type === Byte) {
                numberPropertyNames.push(propertyName);
                createNumberInput(property);
            } else if (modelProperty.type === Decimal) {
                numberPropertyNames.push(propertyName);
                createNumberInput(property);
            } else if (modelProperty.type === DateTimeOffset) {
                datePropertyNames.push(propertyName);
                createDateInput(property);
            } else if (modelProperty.type === Date) {
                datePropertyNames.push(propertyName);
                createDateInput(property);
            } else if (modelProperty.type === Location) {
                throw new Error("Not yet implemented yet.");
            } else if (modelProperty.type === Enum) {
                createEnumInput(modelProperty, property);
            } else if (modelProperty.type === EnumFlag) {
                createEnumFlagInput(modelProperty, property);
            }

            if (property.inputComponent != null) {
                accumulated[propertyName] = property;
            }

            return accumulated;
        }, {});

    };

    TypeDisplay.prototype.labelList = function () {
        return prettifyName(this.model.collectionName);
    };

    TypeDisplay.prototype.labelInstance = function () {
        return prettifyName(this.model.collectionName);
    };

    TypeDisplay.prototype.displayList = function (array) {
        return array.map(this.displayInstance).join(", ");
    };

    TypeDisplay.prototype.displayInstance = function (entity) {
        return entity;
    };

    TypeDisplay.prototype.createRelationshipProperties = function (edm, model) {
        var instance = new model.type();
        var relationships = {};
        var oneToOne = edm.getOneToOneRelationships(instance);
        var oneToMany = edm.getOneToManyRelationships(instance);
        var manyToMany = edm.getManyToManyRelationships(instance);
        var oneToOneTargets = edm.getOneToOneAsTargetRelationships(instance);
        var oneToManyTargets = edm.getOneToManyAsTargetRelationships(instance);
        var manyToManyTargets = edm.getManyToManyAsTargetRelationships(instance);

        oneToOne.forEach(function (relationship) {
            relationships[relationship.hasOne] = {
                name: "app/components/inputs/OneToOneInput.html"
            };
        });

        oneToMany.forEach(function (relationship) {
            relationships[relationship.hasMany] = {
                name: "app/components/inputs/OneToManyInput.html"
            };
        });

        manyToMany.forEach(function (relationship) {
            relationships[relationship.hasMany] = {
                name: "app/components/inputs/ManyToManyInput.html"
            };
        });

        oneToOneTargets.forEach(function (relationship) {
            relationships[relationship.withOne] = {
                name: "app/components/inputs/OneToOneTargetInput.html"
            };
        });

        oneToManyTargets.forEach(function (relationship) {
            relationships[relationship.withOne] = {
                name: "app/components/inputs/OneToManyTargetInput.html"
            };
        });

        manyToManyTargets.forEach(function (relationship) {
            relationships[relationship.withMany] = {
                name: "app/components/inputs/ManyToManyTargetInput.html"
            };
        });

        return relationships;
    };

    app.model.TypeDisplay = TypeDisplay;

});