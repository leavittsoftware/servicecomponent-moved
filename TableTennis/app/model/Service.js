﻿BASE.require([
    "app.model.Edm",
    "app.model.Match",
    "app.model.Game",
    "app.model.ChallengerScore",
    "app.model.DefenderScore",
    "app.model.Player",
    "BASE.data.databases.Sqlite",
    "BASE.data.services.SqliteService"
], function () {

    var Edm = app.model.Edm;
    var Match = app.model.Match;
    var Game = app.model.Game;
    var ChallengerScore = app.model.ChallengerScore;
    var DefenderScore = app.model.DefenderScore;
    var Match = app.model.Match;
    var Player = app.model.Player;
    var Sqlite = BASE.data.databases.Sqlite;
    var Service = BASE.data.services.SqliteService;

    BASE.namespace("app.model");

    app.model.Service = function () {
        var edm = new Edm();

        var database = new Sqlite({
            name: "table_tennis",
            edm: edm
        });

        Service.call(this, database);
    };

    BASE.extend(app.model.Service, Service);

});