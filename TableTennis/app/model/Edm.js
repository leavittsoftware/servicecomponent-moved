﻿BASE.require([
    "BASE.data.Edm",
    "app.model.Match",
    "app.model.Game",
    "app.model.ChallengerScore",
    "app.model.DefenderScore",
    "app.model.Player",
    "Date.prototype.format"
], function () {

    var Future = BASE.async.Future;
    var Edm = BASE.data.Edm;
    var Match = app.model.Match;
    var Game = app.model.Game;
    var ChallengerScore = app.model.ChallengerScore;
    var DefenderScore = app.model.DefenderScore;
    var Match = app.model.Match;
    var Player = app.model.Player;

    var stringValidation = function (value) {
        if (typeof value === "string") {
            return Future.fromResult(value);
        } else {
            return Future.fromError("Not a string.");
        }
    };

    var numberValidation = function (value) {
        if (typeof value === "number") {
            return Future.fromResult(value);
        } else {
            return Future.fromError("Not a number.");
        }
    };

    var defaultDisplay = function (value) {
        return value.toString();
    };

    var displayDate = function (value) {
        return value.format("mm/dd/yyyy");
    };

    BASE.namespace("app.model");

    app.model.Edm = function () {
        Edm.call(this);

        this.addModel({
            type: Match,
            collectionName: "matches",
            properties: {
                id: {
                    type: Integer,
                    primaryKey: true,
                    autoIncrement: true,
                },
                name: {
                    type: String
                }
            }
        });


        this.addModel({
            type: Game,
            collectionName: "games",
            properties: {
                id: {
                    type: Integer,
                    primaryKey: true,
                    autoIncrement: true
                },
                matchId: {
                    type: Integer
                },
                date: {
                    type: Date
                }
            }
        });

        this.addModel({
            type: ChallengerScore,
            collectionName: "challengerScores",
            properties: {
                id: {
                    type: Integer,
                    primaryKey: true,
                    autoIncrement: true
                },
                gameId: {
                    type: Integer
                },
                playerId: {
                    type: Integer
                },
                value: {
                    type: Integer
                }
            }
        });

        this.addModel({
            type: DefenderScore,
            collectionName: "defenderScores",
            properties: {
                id: {
                    type: Integer,
                    primaryKey: true,
                    autoIncrement: true
                },
                gameId: {
                    type: Integer
                },
                playerId: {
                    type: Integer
                },
                value: {
                    type: Integer
                }
            }
        });

        this.addModel({
            type: Player,
            displayName: "Players",
            collectionName: "players",
            properties: {
                id: {
                    type: Integer,
                    primaryKey: true,
                    autoIncrement: true
                },
                firstName: {
                    type: String
                },
                lastName: {
                    type: String
                }
            }
        });

        this.addOneToMany({
            type: Match,
            hasKey: "id",
            hasMany: "games",
            ofType: Game,
            withKey: "id",
            withForeignKey: "matchId",
            withOne: "match"
        });

        this.addOneToOne({
            type: Game,
            hasKey: "id",
            hasOne: "challengerScore",
            ofType: ChallengerScore,
            withKey: "id",
            withForeignKey: "gameId",
            withOne: "game"
        });

        this.addOneToOne({
            type: Game,
            hasKey: "id",
            hasOne: "defenderScore",
            ofType: DefenderScore,
            withKey: "id",
            withForeignKey: "gameId",
            withOne: "game"
        });

        this.addOneToMany({
            type: Player,
            hasKey: "id",
            hasMany: "defenderScores",
            ofType: DefenderScore,
            withKey: "id",
            withForeignKey: "playerId",
            withOne: "player"
        });

        this.addOneToMany({
            type: Player,
            hasKey: "id",
            hasMany: "challengerScores",
            ofType: ChallengerScore,
            withKey: "id",
            withForeignKey: "playerId",
            withOne: "player"
        });
    };


    BASE.extend(app.model.Edm, Edm);

});