﻿BASE.namespace("app.model");

app.model.Player = function () {
    this.id = null;
    this.firstName = null;
    this.lastName = null;
    this.challengerScores = [];
    this.defenderScores = [];
};

