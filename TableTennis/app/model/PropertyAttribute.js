﻿BASE.namespace("app.model");

app.model.PropertyAttribute = function () {
    this.id = null;
    this.key = null;
    this.value = null;
    this.propertyId = null;
    this.property = null;
};