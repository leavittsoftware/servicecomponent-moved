﻿BASE.require([
    "app.model.DisplayService",
    "app.model.Service",
    "app.model.Match",
    "app.model.Game",
    "app.model.Player",
    "app.model.ChallengerScore",
    "app.model.DefenderScore",
    "Date.prototype.format"
], function () {
    var DisplayService = app.model.DisplayService;
    var Service = app.model.Service;
    var Match = app.model.Match;
    var Game = app.model.Game;
    var Player = app.model.Player;
    var ChallengerScore = app.model.ChallengerScore;
    var DefenderScore = app.model.DefenderScore;

    BASE.namespace("app.model");

    app.model.TableTennisDisplay = function () {
        var self = this;
        var service = new app.model.Service;

        app.model.DisplayService.call(self, service);

        var matchesDisplay = {
            type: Match,
            labelInstance: function () {
                return "Match";
            },
            labelList: function () {
                return "Matches";
            },
            displayInstance: function (match) {
                if (match == null) {
                    return "(None)";
                }
                return match.name;
            },
            displayList: function (array) {
                return array.map(matchesDisplay.displayInstance).join(", ");
            },
            search: function (text, orderBy) {
                var queryable = service.asQueryable(Match).where(function (expBuilder) {
                    return expBuilder.property("name").contains(text);
                });

                if (orderBy) {
                    queryable.orderBy(function (expBuilder) {
                        return expBuilder.property(orderBy);
                    });
                }

                return queryable;
            },
            properties: {
                name: {
                    label: function () {
                        return "Name"
                    },
                    display: function (value) {
                        return value;
                    },
                    inputComponent: {
                        name: "app/components/inputs/Text.html"
                    }

                },
                games: {
                    showOnList: false,
                    label: function () {
                        return "Games";
                    },
                    display: function (games) {
                        return gamesDisplay.displayList(games);
                    },
                    inputComponent: {
                        name: "app/components/inputs/OneToManyInput.html"
                    }
                }
            }
        };

        var gamesDisplay = {
            type: Game,
            labelInstance: function () {
                return "Game";
            },
            labelList: function () {
                return "Games";
            },
            displayInstance: function (game) {
                if (game == null) {
                    return "No Game";
                }
                return challengerScoreDisplay.displayInstance(game.challengerScore) + ", " +
                 defenderScoreDisplay.displayInstance(game.defenderScore);
            },
            displayList: function (array) {
                return array.map(gamesDisplay.displayInstance).join(", ");
            },
            search: function (text, orderBy) {
                if (text === "") {
                    return service.asQueryable(Game).include(function (expBuilder) {
                        return expBuilder.property("challengerScore").property("player");
                    }).include(function (expBuilder) {
                        return expBuilder.property("defenderScore").property("player");
                    });
                }

                var queryable = service.asQueryable(Game).where(function (expBuilder) {
                    return expBuilder.or(
                        expBuilder.property("challengerScore").property("player").contains(text),
                        expBuilder.property("defenderScore").property("player").contains(text)
                        );
                }).include(function (expBuilder) {
                    return expBuilder.property("challengerScore").property("player");
                }).include(function (expBuilder) {
                    return expBuilder.property("defenderScore").property("player");
                });

                if (orderBy) {
                    queryable.orderBy(function (expBuilder) {
                        return expBuilder.property(orderBy);
                    });
                }

                return queryable;
            },
            properties: {
                date: {
                    label: function () {
                        return "Date"
                    },
                    display: function (value) {
                        if (value == null) {
                            return "";
                        }

                        return value.format("mm/dd/yyyy");
                    },
                    inputComponent: {
                        name: "app/components/inputs/Date.html"
                    }

                },
                challengerScore: {
                    label: function () {
                        return "Challenger Score"
                    },
                    display: function (challengerScore) {
                        return challengerScoreDisplay.displayInstance(challengerScore);
                    },
                    inputComponent: {
                        name: "app/components/inputs/OneToOneInput.html"
                    }
                },
                defenderScore: {
                    label: function () {
                        return "Defender Score"
                    },
                    display: function (defenderScore) {
                        return defenderScoreDisplay.displayInstance(defenderScore);
                    },
                    inputComponent: {
                        name: "app/components/inputs/OneToOneInput.html"
                    }
                },
                match: {
                    label: function () {
                        return "Match"
                    },
                    display: function (match) {
                        matchesDisplay.displayInstance(match);
                    },
                    inputComponent: {
                        name: "app/components/inputs/OneToManyTargetInput.html"
                    }
                }
            }
        };

        var challengerScoreDisplay = {
            type: ChallengerScore,
            labelInstance: function () {
                return "Challenger Score";
            },
            labelList: function () {
                return "Challenger Scores";
            },
            displayInstance: function (challengerScore) {
                if (challengerScore == null) {
                    return "No Challenger";
                }

                return playerDisplay.displayInstance(challengerScore.player) + ": " +
                    challengerScore.value;
            },
            displayList: function (array) {
                return array.map(challengerScoreDisplay.displayInstance).join(", ");
            },
            search: function (text, orderBy) {
                var queryable = service.asQueryable(ChallengerScore).where(function (expBuilder) {
                    return expBuilder.or(
                        expBuilder.property("player").property("firstName").contains(text),
                        expBuilder.property("player").property("lastName").contains(text)
                        );
                }).include(function (expBuilder) {
                    return expBuilder.property("player");
                });

                if (orderBy) {
                    queryable = queryable.orderBy(function (expBuilder) {
                        return expBuilder.property(orderBy);
                    });
                }

                return queryable;
            },
            properties: {
                value: {
                    label: function () {
                        return "Score"
                    },
                    display: function (value) {
                        return value;
                    },
                    inputComponent: {
                        name: "app/components/inputs/Text.html"
                    }
                },
                player: {
                    label: function () {
                        return "Player"
                    },
                    display: function (player) {
                        return playerDisplay(player);
                    },
                    inputComponent: {
                        name: "app/components/inputs/OneToManyTargetInput.html"
                    }
                }
            }
        };

        var defenderScoreDisplay = {
            type: DefenderScore,
            labelInstance: function () {
                return "Defender Score";
            },
            labelList: function () {
                return "Defender Scores"
            },
            displayInstance: function (defenderScore) {
                if (defenderScore == null) {
                    return "No Defender";
                }

                return playerDisplay.displayInstance(defenderScore.player) + ": " +
                    defenderScore.value;
            },
            displayList: function (array) {
                return array.map(defenderScoreDisplay.displayInstance).join(", ");
            },
            search: function (text, orderBy) {
                var queryable = service.asQueryable(DefenderScore).where(function (expBuilder) {
                    return expBuilder.or(
                        expBuilder.property("player").property("firstName").contains(text),
                        expBuilder.property("player").property("lastName").contains(text)
                        );
                }).include(function (expBuilder) {
                    return expBuilder.property("player");
                });

                if (orderBy) {
                    queryable.orderBy(function (expBuilder) {
                        return expBuilder.property(orderBy);
                    });
                }

                return queryable;
            },
            properties: {
                value: {
                    label: function () {
                        return "Score"
                    },
                    display: function (value) {
                        return value;
                    },
                    inputComponent: {
                        name: "app/components/inputs/Text.html"
                    }
                },
                player: {
                    label: function () {
                        return "Player"
                    },
                    display: function (player) {
                        return playerDisplay(player);
                    },
                    inputComponent: {
                        name: "app/components/inputs/OneToManyTargetInput.html"
                    }
                }
            }
        };

        var playerDisplay = {
            type: Player,
            labelInstance: function () {
                return "Player";
            },
            labelList: function () {
                return "Players"
            },
            displayInstance: function (player) {
                if (player == null) {
                    return "(None)";
                }

                return player.firstName + " " + player.lastName;
            },
            displayList: function (array) {
                return array.map(playerDisplay.displayInstance).join(", ");
            },
            search: function (text, orderBy) {
                var queryable = service.asQueryable(Player).where(function (expBuilder) {
                    return expBuilder.or(
                        expBuilder.property("firstName").contains(text),
                        expBuilder.property("lastName").contains(text)
                        );
                });

                if (orderBy) {
                    queryable = queryable.orderBy(function (expBuilder) {
                        return expBuilder.property(orderBy);
                    });
                }

                return queryable;
            },
            properties: {
                firstName: {
                    label: function () {
                        return "First Name"
                    },
                    display: function (value) {
                        return value;
                    },
                    inputComponent: {
                        name: "app/components/inputs/Text.html"
                    }
                },
                lastName: {
                    label: function () {
                        return "Last Name"
                    },
                    display: function (value) {
                        return value;
                    },
                    inputComponent: {
                        name: "app/components/inputs/Text.html"
                    }
                }
            }
        };

        self.addDisplay(matchesDisplay);
        self.addDisplay(gamesDisplay);
        self.addDisplay(challengerScoreDisplay);
        self.addDisplay(defenderScoreDisplay);
        self.addDisplay(playerDisplay);

    };

    BASE.extend(app.model.TableTennisDisplay, DisplayService);

});