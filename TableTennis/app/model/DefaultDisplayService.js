﻿BASE.require([
    "app.model.DisplayService",
    "app.model.TypeDisplay"
], function () {

    var DisplayService = app.model.DisplayService;
    var TypeDisplay = app.model.TypeDisplay;

    app.model.DefaultDisplayService = function (service) {
        var self = this;

        DisplayService.call(self, service);

        var models = service.getEdm().getModels().getValues();

        models.forEach(function (model) {
            var typeDisplay = new TypeDisplay(model.type, service);
            self.addDisplay(typeDisplay);
        });
    };

    BASE.extend(app.model.DefaultDisplayService, DisplayService);

});