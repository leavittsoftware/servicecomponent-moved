﻿BASE.require([
    "BASE.collections.Hashmap"
], function () {

    var Hashmap = BASE.collections.Hashmap;

    BASE.namespace("app.model");


    app.model.DisplayService = function (service) {
        var self = this;
        var displays = new Hashmap();
        var cachedRelationships = new Hashmap();

        self.service = service;
        self.edm = service.getEdm();

        self.addDisplay = function (display) {
            displays.add(display.type, display);
        };

        self.removeDisplay = function (display) {
            displays.remove(display.type);
        };

        self.getDisplayByType = function (Type) {
            return displays.get(Type);
        };

        self.getDisplays = function () {
            return displays.clone();
        };

        self.getRelationshipsByType = function (Type) {
            var relationships = cachedRelationships.get(Type);
            if (relationships) {
                return relationships;
            }

            relationships = {};
            var instance = new Type();
            var edm = self.edm;

            edm.getOneToOneRelationships(instance).forEach(function (relationship) {
                relationships[relationship.hasOne] = relationship;
            });

            edm.getOneToManyRelationships(instance).forEach(function (relationship) {
                relationships[relationship.hasMany] = relationship;
            });

            edm.getManyToManyRelationships(instance).forEach(function (relationship) {
                relationships[relationship.hasMany] = relationship;
            });

            edm.getOneToOneAsTargetRelationships(instance).forEach(function (relationship) {
                relationships[relationship.withOne] = relationship;
            });

            edm.getOneToManyAsTargetRelationships(instance).forEach(function (relationship) {
                relationships[relationship.withOne] = relationship;
            });

            edm.getManyToManyAsTargetRelationships(instance).forEach(function (relationship) {
                relationships[relationship.withMany] = relationship;
            });

            cachedRelationships.add(Type, relationships);

            return relationships;
        };
    };

});