﻿BASE.namespace("app.model");

app.model.ValueType = function () {
    this.id = null;
    this.name = null;
    this.namespace = null;
    this.properties = [];
};