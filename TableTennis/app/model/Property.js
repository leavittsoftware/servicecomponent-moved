﻿BASE.namespace("app.model");

app.model.Property = function () {
    this.id = null;
    this.name = null;
    this.entityId = null;
    this.entity = null;
    this.valueType = null;
    this.valueTypeId = null;
    this.isNullable = null;
    this.isPrimaryKey = null;
    this.isAutoIncrement = null;
    this.attributes = [];
    this.oneToOnesAsSource = [];
    this.oneToManysAsSource = [];
    this.manyToManysAsSource = [];
    this.oneToOnesAsTarget = [];
    this.oneToManysAsTarget = [];
    this.manyToManysAsTarget = [];
};