﻿BASE.require([
    "jQuery",
    "Array.prototype.asQueryable",
    "requestAnimationFrame",
    "BASE.collections.Hashmap",
    "BASE.web.animation.ElementAnimation"
], function () {

    BASE.namespace("components.material.layouts");

    var Future = BASE.async.Future;
    var Hashmap = BASE.collections.Hashmap;
    var ElementAnimation = BASE.web.animation.ElementAnimation;

    var requestFrameAsync = function () {
        return new Future(function (setValue) {
            requestAnimationFrame(setValue);
        });
    };

    var fadeIn = function (element, duration) {
        var animation = new ElementAnimation({
            target: element,
            properties: {
                opacity: {
                    from: 0,
                    to: 1
                }
            },
            easing: "easeOutExpo",
            duration: duration
        });

        return animation;
    };

    var fadeOut = function () {
        var animation = new ElementAnimation({
            target: element,
            properties: {
                opacity: {
                    from: 1,
                    to: 0
                }
            },
            easing: "easeOutExpo",
            duration: duration
        });

        return animation;
    };

    var recoverFuture = function (future) {
        return future.catchCanceled(emptyFn).catch(emptyFn);
    };

    var invokeMethodIfExist = function (controller, methodName, args) {
        var returnValue = emptyFuture;

        if (!Array.isArray(args)) {
            args = [];
        }

        if (controller) {
            var method = controller[methodName];
            if (typeof method === "function") {
                returnValue = method.apply(controller, args);

                if (!(returnValue instanceof Future)) {
                    returnValue = Future.fromResult(returnValue);
                }

            }
        }

        return recoverFuture(returnValue);
    };


    components.material.layouts.Collection = function (elem, tags) {
        var self = this;
        var $elem = $(elem);
        var content = tags["content"];
        var feedback = tags["feedback-state-manager"];
        var $content = $(content);
        var $feedback = $(feedback);
        var feedbackController = $feedback.controller();
        var collectionLength = 0;
        var lastSkip = 0;
        var batchSize = 50;
        var originalQueryable;
        var itemPositions = [];
        var entities = [];
        var itemsPool = [];
        var itemsOnDom = new Hashmap();
        var componentName = $elem.attr("item-component");
        var viewportHeight = 0;
        var lastIndex = 0;

        var initializeValues = function () {
            lastSkip = 0;
            collectionLength = 0;
            itemPositions = [];
            entities = [];
            viewportHeight = elem.offsetHeight;

            itemsOnDom.getValues().forEach(removeItem);
        };

        var removeItem = function ($item) {
            invokeMethodIfExist($item.controller, "prepareToDeactivate");
            itemsOnDom.remove($item);
            $item.detach();
            itemPool.push($item);
            invokeMethodIfExist($item.controller, "deactivated");
        };

        var removeItemsOffViewport = function () {
            var top = elem.scrollTop;
            var bottom = top + viewportHeight;

            itemsOnDom.getValues().forEach(function ($item) {
                var index = $item.data("collection-index");
                var itemTop = itemPositions[index];
                var itemBottom = itemPositions[index + 1];

                if (itemBottom < top || itemTop > bottom) {
                    removeItem($item);
                }
            });
        };

        var createItemAsync = function () {
            return BASE.web.components.createComponent(componentName);
        };

        var getItemAsync = function () {
            if (itemsPool.length === 0) {
                return createItemAsync();
            }

            var $item = itemsPool.pop();
            return Future.fromResult($item);
        };

        var getNextBatchAsync = function () {
            var arrayFuture = originalQueryable.skip(currentSkip).take(batchSize).toArray().chain(function (results) {
                entities = entities.concat(results);
            });
            currentSkip += batchSize;
            return arrayFuture;
        };

        var computeHeightFromIndexAsync = function (fromIndex) {
            return getItemAsync().chain(function ($item) {
                var controller = $item.controller();

                if (!controller || typeof controller.getItemHeight !== "function") {
                    throw new Error("Items need to implement a getItemHeight method.");
                }

                var length = entities.length;
<<<<<<< HEAD
                var nextHeight = 0;
                fromIndex = fromIndex >= 0 ? fromIndex : 0;
=======
                var itemLength = itemPositions.length;
                var nextHeight = 0;
                itemPositions[0] = 0;
>>>>>>> cd96b854127ebc5d6f329d27da5e25c92368aa8e

                for (var x = fromIndex; x < length; x++) {
                    nextHeight = controller.getItemHeight(entities[x]);
                    itemPositions[x + 1] = nextHeight + itemPositions[x];
                }

                var total = itemPositions[itemPositions.length - 1];

                $content.css("height", total + "px");
            });
        };

<<<<<<< HEAD
        var computeHeightsAsync = function () {
            return computeHeightFromIndexAsync(itemPositions.length - 1);
        };

        var getItemPosition = function (index) {
            return itemPositions[index - 1] || 0
        };

=======
>>>>>>> cd96b854127ebc5d6f329d27da5e25c92368aa8e
        var layoutItemsAsync = function () {
            return removeItemsOffViewport().chain(function () {
                return computeHeightsAsync();
            }).chain(function () {
                return requestFrameAsync();
            }).chain(function () {
                var placementFutures = [];

                var top = elem.scrollTop;
                var bottom = top + viewportHeight;
                var index = 0;
                var currentY = itemPositions[lastIndex];

                var alreadyOnDom = itemsOnDom.reduce(function (hash, $item) {
                    hash[$item.data("collection-index")] = $item;
                }, {});

                // Find where to start from our last index.
                if (currentY > top) {
                    while (currentY > top) {
                        currentY = itemPositions[index];
                        index--;
                    }
                } else {
                    while (currentY < top) {
                        currentY = itemPositions[index];
                        index++;
                    }
                }

                lastIndex = index;

                // Go until we hit the bottom of the viewport.
                while (currentY <= bottom) {
                    currentY = itemPositions[index];

                    if (!alreadyOnDom[index]) {
                        continue;
                    }

                    (function (currentY, index) {
                        placementFutures.push(getItemAsync().chain(function ($item) {
                            var controller = $item.controller();
                            invokeMethodIfExist(controller, "prepareToActivate");

                            $item.css({
                                transform: "translateY(" + currentY + "px)"
                            });

                            $item.appendTo(elem).data("collection-index", index);
                            invokeMethodIfExist(controller, "activated", entities[x]);
                        }));
                    })(currentY, index);

                    index++;
                }

                return Future.all(placementFutures);
            });

        };

<<<<<<< HEAD
        var fetchAsync = function () {
            var arrayFuture = getNextBatchAsync();

            return Future.all([arrayFuture, feedbackController.pushAsync("loading")]).catch(function (error) {
                feedbackController.pushAsync("error");
            }).chain(function () {
                return computeHeightsAsync();
            }).chain(function () {
                return layoutItemsAsync();
            });
        };

=======
>>>>>>> cd96b854127ebc5d6f329d27da5e25c92368aa8e
        self.setQueryableAsync = function (queryable) {
            initializeValues();
            originalQueryable = queryable;

            var arrayFuture = getNextBatchAsync();

            return Future.all([arrayFuture, feedbackController.pushAsync("loading")]).catch(function (error) {
                feedbackController.pushAsync("error");
            });
        };

    };
});