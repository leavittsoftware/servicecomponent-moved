﻿BASE.require([
    "jQuery"
], function () {
    BASE.namespace("demos.materialInputs");
    var Future = BASE.async.Future;

    demos.materialInputs = function () {
        var self = this;
        var $validateButton = $("#validate-button");

        $validateButton.on("click", function(index, element) {
            $(".input-validate").each(function(index, element) {
                $(element).controller().validateAsync()["try"]();

                console.log($(element).controller().getValue());
                //$(element).controller().reset();
            });
        });
    }
});