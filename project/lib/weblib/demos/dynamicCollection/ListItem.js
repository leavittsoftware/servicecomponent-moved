﻿BASE.require(["jQuery"], function () {
    ListItem = function (elem, tags, service) {
        var self = this;
        var $elem = $(elem);
        var $title = $(tags["title"]);
        var $container = $(tags["container"]);

        self.getItemHeight = function (item) {
            var char = item.firstName.substring(0, 1);
            if (char === "J") {
                return 100;
            } else if (char === "G") {
                return 200;
            } else if (char === "B") {
                return 300;
            } else {
                return 150;
            }
        };

        self.activated = function (item) {

            var char = item.firstName.charAt(0);
            var color = "green";

            if (char === "J") {
                color = "red";
            } else if (char === "G") {
                color = "green";
            } else if (char === "B") {
                color = "blue"
            } else {
                color = "orange"
            }

            $container.css("backgroundColor", color);
            $title.text(item.firstName);
        };
    };
});