﻿BASE.require([
    "BASE.query.Provider",
    "BASE.query.ArrayVisitor",
    "BASE.query.Queryable",
    "BASE.async.Future",
    "BASE.async.Task",
    "BASE.async.delay"
], function () {
    BASE.namespace("demos.testing");

    var ArrayVisitor = BASE.query.ArrayVisitor;
    var Queryable = BASE.query.Queryable;
    var Provider = BASE.query.Provider;
    var Future = BASE.async.Future;
    var delay = BASE.async.delay;

    demos.testing.MockArrayProvider = (function (Super) {
        var ArrayProvider = function (array) {
            var self = this;
            BASE.assertNotGlobal(self);

            Super.call(self, array);

            var error = null;
            var latencyTime = 0;

            var errorState = {
                toArray: function (queryable) {
                    currentState = latencyState;
                    return delay(latencyTime).chain(function () {
                        return Future.fromError(error);
                    });
                }
            };

            var toArray = function (queryable) {
                return new Future(function (setValue, setError) {
                    var Type = queryable.Type;
                    var visitor = new ArrayVisitor();

                    var expression = queryable.getExpression();

                    var filter = null;
                    var sort = null;
                    var skip = 0;
                    var take = null;
                    var results = null;

                    if (expression.where !== null) {
                        filter = visitor.parse(expression.where);
                    }

                    if (expression.orderBy !== null) {
                        sort = visitor.parse(expression.orderBy);
                    }

                    if (expression.skip !== null) {
                        skip = expression.skip.children[0].value;
                    }

                    if (expression.take !== null) {
                        take = expression.take.children[0].value;
                    }

                    if (filter) {
                        results = array.filter(filter);
                    } else {
                        results = array.slice(0);
                    }

                    if (sort) {
                        results = results.sort(sort);
                    }

                    if (take === null) {
                        take = undefined;
                    } else {
                        take = skip + take;
                    }

                    results = results.slice(skip, take);

                    setTimeout(function () {
                        setValue(results);
                    }, 0);
                });
            };

            var latencyState = {
                toArray: function (queryable) {
                    return delay(latencyTime).chain(function () {
                        return toArray(queryable);
                    });
                }
            };

            var currentState = latencyState;

            self.throwErrorOnNextQuery = function (newError) {
                error = newError;
                currentState = errorState;
            };

            self.addLatencyToNextQuery = function (latency) {
                latencyTime = latency;
                currentState = latencyState;
            };

            self.toArray = function (queryable) {
                return currentState.toArray(queryable);
            };

            self.execute = self.toArray;
        };

        BASE.extend(ArrayProvider, Super);

        return ArrayProvider;
    }(BASE.query.Provider));

    Array.prototype.asQueryableMock = function () {
        var queryable = new Queryable();
        queryable.provider = new demos.testing.MockArrayProvider(this);
        return queryable;
    };

});