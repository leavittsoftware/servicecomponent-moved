﻿BASE.require([
    "jQuery"
], function () {
    BASE.namespace("demos.multiSelect");

    demos.multiSelect.Item = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $name = $(tags["name"]);
        var $clear = $(tags['clear']);
        var item = null;

        self.setItem = function (entity) {
            $name.text(entity.firstName + " " + entity.lastName)
            item = entity;
        };

        $clear.on('click', function () {
            $elem.trigger({
                type: 'remove-item',
                item: item,
                element: elem
            })
        });

    };
});