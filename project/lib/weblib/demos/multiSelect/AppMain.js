﻿BASE.require([
    'jQuery',
    'demos.testing.MockArrayProvider',
    'BASE.query.Queryable'
], function () {
    BASE.namespace('demos.multiSelect');

    var Queryable = BASE.query.Queryable;

    // Create an array to make a queryable with.
    var list = [{
        id: 1,
        firstName: "Jared",
        lastName: "Barnes"
    },
    {
        id: 2,
        firstName: "Justin",
        lastName: "Barnes"
    },
    {
        id: 3,
        firstName: "LeAnn",
        lastName: "Barnes"
    },
    {
        id: 4,
        firstName: "Ben",
        lastName: "Howe"
    },
    {
        id: 5,
        firstName: "Ashley",
        lastName: "Howe"
    }];

    var clone = list.slice(0);

    var provider = new demos.testing.MockArrayProvider(clone);

    demos.multiSelect.AppMain = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var multiSelectController = $(tags['multi-select']).controller();

        multiSelectController.setValue([]);

        multiSelectController.setSearchQueryable = function(search) {
            var queryable = new Queryable();
            var multiSelectValueArray = multiSelectController.getValue();
            var selectedIds = multiSelectValueArray.map(function (person) {
                return person.id;
            });

            queryable.provider = provider;

            queryable = queryable.where(function (e) {
                var ands = selectedIds.reduce(function (ands, id) {
                    ands.push(e.property("id").isNotEqualTo(id));
                    return ands;
                }, []);

                ands.push(e.property("firstName").contains(search));
                return e.and.apply(e, ands);
            });

            return queryable;
        };

        multiSelectController.setStringForSearchItem = function (item) {
            return item.firstName + " " + item.lastName;
        };
    };
});