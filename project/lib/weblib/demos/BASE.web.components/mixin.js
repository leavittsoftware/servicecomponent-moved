﻿(function () {
    var Future = BASE.async.Future;
    BASE.namespace("demo");

    demo.mixin = function (elem, tags, scope) {
        var self = this;

        self.registerValidator(function (value) {

            value = parseInt(value, 10);

            if (isNaN(value) || typeof value !== "number") {
                return Future.fromError(new Error("This input requires a number."));
            }

            return Future.fromResult();

        });

        self.jump = function () {
            console.log("JUMP");
        };
    };
}());