﻿(function () {
    var Future = BASE.async.Future;
    BASE.namespace("demo");

    demo.mixin2 = function (elem, tags, scope) {
        var self = this;

        self.registerValidator(function (value) {

            if (value === "") {
                return Future.fromError(new Error("Can't be an empty string."));
            }

            return Future.fromResult();

        });

    };
}());