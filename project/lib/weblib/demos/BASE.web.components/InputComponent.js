﻿BASE.require(["jQuery"], function () {
    var Future = BASE.async.Future;

    BASE.namespace("demo");

    demo.InputComponent = function (elem, tags) {
        var self = this;
        var validators = [];
        var $input = $(tags["input"]);

        self.registerValidator = function (validator) {
            if (typeof validator !== "function") {
                throw new Error("Invalid Argument: Validator needs to be a function.");
            }
            validators.push(validator);
        };

        self.validate = function () {
            var value = $input.val();
            return validators.reduce(function (previouseValidationFuture, validator) {
                return previouseValidationFuture.chain(function () {
                    return validator(value);
                });
            }, Future.fromResult());
        };
    };
});