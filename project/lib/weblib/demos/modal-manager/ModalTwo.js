﻿BASE.require([
    "jQuery"
], function () {
    BASE.namespace("modalManagerDemo");

    modalManagerDemo.ModalTwo = function (elem, tags, scope) {
        var self = this;

        self.activated = function () {
            console.log("Two activated.");
        };

        self.deactivated = function () {
            console.log("Two deactivated.");
        };

        self.prepareToActivateAsync = function () {
            console.log("Two prepareToActivateAsync.");
        };

        self.prepareToDeactivateAsync = function () {
            console.log("Two prepareToDeactivateAsync.");
        };
    };

});