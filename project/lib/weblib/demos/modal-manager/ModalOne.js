﻿BASE.require([
    "jQuery",
    "components.material.animations.createFadeInAnimation"
], function () {
    var createFadeInAnimation = components.material.animations.createFadeInAnimation;

    BASE.namespace("modalManagerDemo");

    modalManagerDemo.ModalOne = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $close = $(tags["close"]);
        var modalManager = null;

        $veil = $(tags["veil"]);

        var fadeInAnimation = createFadeInAnimation(elem, 350);

        var showModalAsync = function () {
            return fadeInAnimation.playToEndAsync();
        };

        var hideModalAsync = function () {
            return fadeInAnimation.reverseToStartAsync();
        };

        self.activated = function (options) {
            showModalAsync()["try"]();
        };

        self.deactivated = function (options) {
        };

        self.prepareToActivateAsync = function (options) {
            $elem.css({
                opacity: 0
            });
        };

        self.prepareToDeactivateAsync = function (options) {
            return hideModalAsync();
        };

        self.setModalManager = function (value) {
            modalManager = value;
        };

        $veil.on("click", function () {
            modalManager.hideAsync()["try"]();
        });

        $close.on("click", function () {
            modalManager.hideAsync()["try"]();
        });
    };

});