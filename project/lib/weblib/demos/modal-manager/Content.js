﻿BASE.require([
    "jQuery",
    "BASE.async.Fulfillment"
], function () {
    var Fulfillment = BASE.async.Fulfillment;

    BASE.namespace("modalManagerDemo");

    modalManagerDemo.Content = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);

        var $showOne = $(tags["showOne"]);
        var $hideOne = $(tags["hideOne"]);

        $showOne.on("click", function () {
            $elem.getModalAsync("one").then(function (modalManager) {
                modalManager.showAsync().then(function (value) { console.log(value); });
            });
        });

        $hideOne.on("click", function () {
            $elem.getModalAsync("one").then(function (modalManager) {
                modalManager.hideAsync().then(function (value) { console.log(value); });
            });
        });


    };
});