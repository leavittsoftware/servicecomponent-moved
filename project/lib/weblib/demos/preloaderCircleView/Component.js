﻿BASE.require([
    "jQuery"
], function () {
    BASE.namespace('demos.preloaderCircleView');

    demos.preloaderCircleView.Component = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var preloaderCircleViewController = $(tags['preloader-circle-view']).controller();

        preloaderCircleViewController.setCircumference(300);

        preloaderCircleViewController.setBorderWidth(50);

        setTimeout(function () {
            preloaderCircleViewController.pause();
        }, 3000);

        setTimeout(function () {
            preloaderCircleViewController.play();
        }, 8000);

    }
});