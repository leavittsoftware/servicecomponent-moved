﻿BASE.require([
    "jQuery",
    "components.material.segues.SlideRightToLeftSoft"
], function () {
    BASE.namespace('demos.lazyLoadState');

    demos.lazyLoadState.First = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var SlideRightToLeftSoft = new components.material.segues.SlideRightToLeftSoft()
        var $updateState = $(tags['update-state']);

        self.init = function (stateManager) {
            self.stateManager = stateManager;
        }

        self.prepareToActivateAsync = function () {
            console.log('first prepare to activate');
        };

        self.activated = function () {
            console.log('first activated');
        };

        self.prepareToDeactivateAsync = function () {
            console.log('first prepare to deactivate');
        };

        self.deactivated = function () {
            console.log('first deactivated');
        };

        self.updateState = function () {
            console.log('first state updated')
        }

        $elem.on('click', function () {
            self.stateManager.pushAsync('second', { segue: SlideRightToLeftSoft })["try"]();
        });

        $updateState.on('click', function (event) {
            event.stopPropagation();
            self.stateManager.pushAsync('first', { segue: SlideRightToLeftSoft })["try"]();
        });
    }
});