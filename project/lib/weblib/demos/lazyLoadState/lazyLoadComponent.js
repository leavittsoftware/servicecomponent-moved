﻿BASE.require([
    "jQuery"
], function () {
    BASE.namespace('demos.lazyLoadState');

    demos.lazyLoadState.LazyLoadComponent = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $second = $(tags['second']);
        var secondController = $second.controller();
        var stateManagerController = $(tags['state-manager']).controller();

        //secondController.getChildElementAsync().then(function (childElement) {
        //    var childElementController = $(childElement).controller();
        //    childElementController.alert();
        //});
       
        stateManagerController.pushAsync('first')["try"]();
    }
});