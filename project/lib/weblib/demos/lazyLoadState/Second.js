﻿BASE.require([
    "jQuery",
    "components.material.segues.SlideRightToLeftSoft",
    "BASE.async.delay"
], function () {
    BASE.namespace('demos.lazyLoadState');

    var Future = BASE.async.Future;

    demos.lazyLoadState.Second = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var SlideRightToLeftSoft = new components.material.segues.SlideRightToLeftSoft();
        var delay = BASE.async.delay;

        self.init = function (stateManager) {
            self.stateManager = stateManager;
        }

        self.prepareToActivateAsync = function () {
            console.log('second prepare to activate');
        };

        self.activated = function () {
            console.log('second actived');
        };

        self.prepareToDeactivateAsync = function () {
            console.log('second prepare to deactivate');
        };

        self.deactivated = function () {
            console.log('second deactivated');
        };

        $elem.on('click', function () {
            self.stateManager.pushAsync('first', { segue: SlideRightToLeftSoft })["try"]();
        });

        self.alert = function(){
            console.log('I work');
        };
    }
})