﻿BASE.require(["jQuery"], function () {

    ListItem = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $firstName = $(tags["first-name"]);
        var $lastName = $(tags["last-name"]);
        var $index = $(tags["index"]);

        self.setItem = function (item, index) {
            $index.text(index);
            $firstName.text(item.firstName);
            $lastName.text(item.lastName);
        };
    };
});