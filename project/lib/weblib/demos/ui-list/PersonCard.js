﻿BASE.require([
    "jQuery",
    "BASE.web.animation.ElementAnimation"
], function () {

    var ElementAnimation = BASE.web.animation.ElementAnimation;

    PersonCard = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $firstName = $(tags["firstName"]);
        var $lastName = $(tags["lastName"]);
        var $title = $(tags["title"]);
        var $image = $(tags["image"]);

        self.setItem = function (item, index) {
            $firstName.text(item.firstName);
            $lastName.text(item.lastName);
            $title.text(item.title);
            //$image[0].src = "";
        };

        $elem.on("click", function () {
            $elem.trigger({
                type: "itemSelected",
                element: elem
            });
            return false;
        });
    };
});