﻿BASE.require(["jQuery"], function () {

    FlowCard = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $left = $(tags["left"]);
        var $right = $(tags["right"]);
        var $middle = $(tags["middle"]);
        var $content = $(tags["content"]);

        self.setItem = function (item, index) {
            var name = item.firstName + " " + item.lastName;

            $left.text(name);
            $right.text(name);
            $middle.text(name);
            $content.text(name);
        };
    };
});