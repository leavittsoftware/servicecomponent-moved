﻿BASE.require([
    "jQuery",
    "BASE.web.animation.ElementAnimation"
], function () {

    var ElementAnimation = BASE.web.animation.ElementAnimation;

    var ElementAnimation = BASE.web.animation.ElementAnimation;

    var firstNames = ["Jared", "Justin", "Ben", "Blake", "Anthony", "Gavin"];
    var lastNames = ["Farnsworth", "Barnes", "Plumb", "Howe", "Rogers", "Leavitt"];
    var titles = ["Manager", "Janitor", "President", "Vice President", "Secretary", "Board Member"];
    var images = [
        "http://goodlifegarden.ucdavis.edu/blog/wp-content/uploads/2011/07/harlequin_bug2.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/3/3f/Crystal_Project_bug.png",
        "http://goodlifegarden.ucdavis.edu/blog/wp-content/uploads/2011/07/harlequin_bug2.jpg",
        "http://www.clker.com/cliparts/c/3/b/d/1194985428453820625bug_nicu_buculei_01.svg.hi.png",
        "https://upload.wikimedia.org/wikipedia/commons/3/3f/Crystal_Project_bug.png",
        "http://www.clker.com/cliparts/c/3/b/d/1194985428453820625bug_nicu_buculei_01.svg.hi.png"
    ];

    var generatePerson = function () {
        var index = parseInt((Math.random() * 100) % firstNames.length);
        var firstName = firstNames[index];
        var lastName = lastNames[index];
        var title = titles[index];
        var image = images[index];

        return {
            firstName: firstName,
            lastName: lastName,
            imageSource: image,
            title: title
        };
    };

    var generateList = function (length) {
        var list = [];
        for (var x = 0 ; x < length ; x++) {
            list.push(generatePerson());
        }
        return list;
    };


    PeopleList = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var stateManagerController = $(tags["state-manager"]).controller();
        var $detail = $(tags["detail"]);
        var list = $(tags["list"]).controller();

        list.setQueryable(generateList(4000).asQueryable());

        stateManagerController.pushAsync("list")["try"]();

        $elem.on("itemSelected", function (event) {
            stateManagerController.pushAsync("detail", event)["try"]();
            return false;
        });
    };
});