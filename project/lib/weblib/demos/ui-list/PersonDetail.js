﻿BASE.require([
    "jQuery",
    "jQuery.fn.region",
    "BASE.web.animation.ElementAnimation",
    "BASE.web.animation.PercentageTimeline"
], function () {
    var ElementAnimation = BASE.web.animation.ElementAnimation;
    var PercentageTimeline = BASE.web.animation.PercentageTimeline;

    var createMockPositionAnimation = function (elem, x, y) {
        var animation = new ElementAnimation({
            target: elem,
            properties: {
                translateX: x,
                translateY: y
            },
            easing: "easeOutExpo",
            duration: 700
        });

        return animation;
    };

    var createVeilAnimation = function (elem) {
        var animation = new ElementAnimation({
            target: elem,
            properties: {
                opacity: {
                    from: 0,
                    to: 0.3
                }
            },
            easing: "linear",
            duration: 700
        });

        return animation;
    };

    var createCallAnimation = function (elem, y) {
        var animation = new ElementAnimation({
            target: elem,
            properties: {
                translateY: y,
                height: {
                    from: "140px",
                    to: "40px"
                },
                backgroundColor: {
                    from: "#fff",
                    to: "#279E27"
                }
            },
            easing: "easeOutExpo",
            duration: 700
        });

        return animation;
    };

    var createImageAnimation = function (elem) {
        var animation = new ElementAnimation({
            target: elem,
            properties: {
                translateY: {
                    from: "50px",
                    to: "80px"
                }
            },
            easing: "easeOutExpo",
            duration: 700
        });

        return animation;
    };

    var createDetailAnimation = function (elem) {
        var animation = new ElementAnimation({
            target: elem,
            properties: {
                translateY: {
                    from: "60px",
                    to: "60px;"
                },
                scaleX: {
                    from: 0.90,
                    to: 1
                },
                scaleY: {
                    from: 0.90,
                    to: 1
                },
                opacity: {
                    from: 0.4,
                    to: 1
                }
            },
            easing: "easeOutExpo",
            duration: 700
        });

        return animation;
    };


    PersonDetail = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var mock = tags["mock-card"];
        var $mockCard = $(mock);
        var veil = tags["veil"];
        var image = tags["image"];
        var imageContainer = tags["image-container"];
        var detailBody = tags["detail-body"];
        var callButton = tags["call-button"];
        var fromX;
        var fromY;

        var veilAnimation = createVeilAnimation(veil);

        var createSegueAnimation = function (options) {
            var $cardElement = $(options.element);

            var parentRegion = $elem.region();
            var cardRegion = $cardElement.region();

            fromX = cardRegion.x - parentRegion.x;
            fromY = cardRegion.y - parentRegion.y;

            $cardElement.css("display", "none");
            $mockCard.css({
                "width": cardRegion.width + "px",
                "height": cardRegion.height + "px"
            });

            var animation = createMockPositionAnimation(mock, {
                from: fromX + "px",
                to: "0px"
            }, {
                from: fromY + "px",
                to: "0px"
            });

            var callYPosition = {
                from: "60px",
                to: (parentRegion.bottom - 35) + "px"
            };

            var detailHeight = parentRegion.height - 200;
            var callAnimation = createCallAnimation(callButton, callYPosition);
            var imageAnimation = createImageAnimation(imageContainer);
            var detailAnimation = createDetailAnimation(detailBody);

            detailBody.style.opacity = 0;

            detailAnimation.observe("start", function () {
                detailBody.style.height = detailHeight + "px"
            });

            var timeline = new PercentageTimeline(700);
            timeline.add({
                animation: animation,
                startAt: 0,
                endAt: 1
            }, {
                animation: veilAnimation,
                startAt: 0,
                endAt: 1
            }, {
                animation: callAnimation,
                startAt: 0,
                endAt: 1
            }, {
                animation: imageAnimation,
                startAt: 0,
                endAt: 1
            }, {
                animation: detailAnimation,
                startAt: 0.3,
                endAt: 1
            });

            return timeline;
        };

        self.activated = function (options) {
            var animation = createSegueAnimation(options);

            animation.play();
        };

        self.prepareToDeactivateAsync = function () {
            var animation = createMockPositionAnimation(mock, {
                from: "0px",
                to: fromX + "px"
            }, {
                from: "0px",
                to: fromY + "px"
            });

            return animation.playToEndAsync();
        };
    };
});