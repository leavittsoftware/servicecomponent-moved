﻿BASE.require([
    "jQuery",
    "components.ui.layouts.collections.ListLayout",
    "Array.prototype.asQueryable"
], function () {

    var ListLayout = components.ui.layouts.collections.ListLayout;

    List = function (elem, tags, scope) {
        var self = this;

        var queryable;
        var $collection = $(tags["collection"]);
        var collectionController = $collection.controller();
        var listItem = $(elem).attr("list-item");

        var listLayout = new ListLayout({
            height: 100
        });

        var buildArray = function () {
            var array = [];
            for (var x = 0 ; x < 1000; x++) {
                array.push({
                    name: "Company " + x
                });
            }
            return array;
        };

        if (typeof listItem !== "string") {
            throw new Error("List needs to have an attribute of list-item to run. Its the list items component name or url.");
        }

        listLayout.component = listItem;
        queryable = buildArray().asQueryable();
        collectionController.setLayout(listLayout);
        collectionController.setQueryable(queryable);

    };

});