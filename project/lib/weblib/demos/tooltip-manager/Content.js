﻿BASE.require([
    "jQuery",
    "BASE.async.Fulfillment",
    "jQuery.fn.region"
], function () {
    var Fulfillment = BASE.async.Fulfillment;

    BASE.namespace("demo");

    demo.Content = function (elem, tags, scope) {
        var $elem = $(elem);

        var $message = $(tags["message"]);
        var $elementMessage = $(tags["element-message"]);
        var $customMessage = $(tags["custom-message"]).remove();

        $message.on("mouseenter", function () {
            var $this = $(this);
            var region = $this.region();

            var position = {
                top: region.top + (region.height / 2),
                left: region.right
            };

            var event = $.Event("tooltip");
            event.tooltipManagerAsync = new Fulfillment();
            event.tooltipTarget = this;

            $elem.trigger(event);

            event.tooltipManagerAsync.then(function (tooltopManagner) {
                tooltopManagner.showAsync({
                    position: position,
                    message: "This here is a blue block.",
                    maxWidth: "100px",
                    duration: 0
                })["try"]();
            });

        });

        $message.on("mouseleave", function () {
            var event = $.Event("tooltip");
            event.tooltipManagerAsync = new Fulfillment();

            $elem.trigger(event);

            event.tooltipManagerAsync.then(function (tooltopManagner) {
                tooltopManagner.hideAsync({ duration: 0 })["try"]();
            });

        });


        $elementMessage.on("mouseenter", function () {
            var $this = $(this);
            var region = $this.region();

            var position = {
                top: region.top + (region.height / 2),
                left: region.right
            };

            var event = $.Event("tooltip");
            event.tooltipManagerAsync = new Fulfillment();

            $elem.trigger(event);

            event.tooltipManagerAsync.then(function (tooltopManagner) {
                tooltopManagner.showAsync({
                    position: position,
                    maxWidth: "100px",
                    messageElement: $customMessage[0],
                    slide: false,
                    duration: 0
                })["try"]();
            });

        });

        $elementMessage.on("mouseleave", function () {
            var event = $.Event("tooltip");
            event.tooltipManagerAsync = new Fulfillment();

            $elem.trigger(event);

            event.tooltipManagerAsync.then(function (tooltopManagner) {
                tooltopManagner.hideAsync({ duration: 0 })["try"]();
            });

        });

    };
});