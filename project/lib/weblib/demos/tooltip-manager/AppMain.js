﻿BASE.require([
    "jQuery",
    "components.material.segues.FadeOutFadeIn",
    "components.material.segues.AppearInstantSegue",
    "components.material.segues.SlideLeftToRightSoft",
    "components.material.segues.SlideRightToLeftSoft",
    "components.material.segues.ZoomOutFadeIn"
], function () {
    var fadeOutFadeIn = new components.material.segues.FadeOutFadeIn();
    var appearInstantSegue = new components.material.segues.AppearInstantSegue();
    var slideLeftToRightSoft = new components.material.segues.SlideLeftToRightSoft();
    var slideRightToLeftSoft = new components.material.segues.SlideRightToLeftSoft();
    var zoomOutFadeIn = new components.material.segues.ZoomOutFadeIn();
    var segue = slideRightToLeftSoft;

    BASE.namespace("demo");

    demo.AppMain = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $modalName = $(tags["modal-name"]);
        var $modalManager = $(tags["tooltip-manager"]);
        var $show = $(tags["show"]);
        var $remove = $(tags["remove"]);
        var $showOne = $(tags["showOne"]);
        var $showTwo = $(tags["showTwo"]);
        var modalManagerController = $modalManager.controller();

        $remove.on("click", function () {
            var modalName = $modalName.val();
            modalManagerController.hideModalAsync(modalName)["try"]();
        });

        $show.on("click", function () {
            var modalName = $modalName.val();
            modalManagerController.showModalAsync(modalName)["try"]();
        });
    };

});