﻿BASE.require([
    "jQuery",
    "BASE.web.animation.ElementAnimation"
], function () {

    BASE.namespace("demos.animations.components");

    var ElementAnimation = BASE.web.animation.ElementAnimation;

    demos.animations.components.EasingAnimation = function (elem, tags, scope) {
        var $easingAnimationBall = $(tags["easing-animation-ball"]);
        var $easingSelector = $(tags["easing-selector"]);
        var easingAnimationController = $(tags["easing-animation"]).controller();
        var easingAnimation = new ElementAnimation({
            target: $easingAnimationBall[0],
            properties: {
                translateX: {
                    from: "0px",
                    to: "700px"
                }
            },
            duration: 3000
        });

        $easingSelector.on("change", function () {
            var value = $easingSelector.val();
            var easing = BASE.web.animation.easings[value];
            easingAnimation._easingFunction = easing;
            easingAnimation.pause();
            easingAnimation.seek(0);
        });

        easingAnimationController.setAnimation(easingAnimation);
    };

});