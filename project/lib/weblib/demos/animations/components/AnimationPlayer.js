﻿BASE.require([
    "jQuery",
    "jQuery.fn.region"
], function () {
    BASE.namespace("demos.animations.components");

    demos.animations.components.AnimationPlayer = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $progressBar = $(tags["progress-bar"]);
        var $progressHandle = $(tags["progress-handle"]);
        var $playButton = $(tags["play-button"]);
        var $icon = $playButton.children().first();
        var handleController = $progressHandle.controller();
        var progressBarRegion;
        var progressHandleWidth;
        var tickObserver = null;
        var animation = null;

        var playState = function () {
            self.pause();
        };

        var pauseState = function () {
            self.pause();
            if (animation !== null && animation._progress === 1) {
                animation.seek(0);
            }
            self.play();
        };

        var state = pauseState;

        var getRegions = function () {
            progressBarRegion = $progressBar.region();
            progressHandleWidth = $progressHandle.width();
        };

        self.setAnimation = function (value) {
            animation = value;

            if (tickObserver !== null) {
                tickObserver.dispose();
            }

            getRegions();

            tickObserver = animation.observe("tick", function (event) {
                var x = (progressBarRegion.width - progressHandleWidth) * event.progress;
                handleController.setPosition(x, 0);

                if (event.progress === 1 || event.progress === 0) {
                    self.pause();
                }

            });

        };

        self.play = function () {
            state = playState;
            $icon.removeClass("icon-play");
            $icon.addClass("icon-pause");

            if (animation !== null) {
                animation.play();
            }
        };

        self.pause = function () {
            state = pauseState;
            $icon.removeClass("icon-pause");
            $icon.addClass("icon-play");

            if (animation !== null) {
                animation.pause();
            }
        };

        handleController.setContainer($progressBar);

        $progressHandle.on("dragStart", function () {
            self.pause();
            getRegions();
        });

        $progressHandle.on("dragging", function () {
            var handleRegion = $progressHandle.region();
            var percentage = 1 - ((progressBarRegion.right - handleRegion.right) / (progressBarRegion.width - progressHandleWidth));

            if (animation !== null) {
                animation.seek(percentage);
            }
        });

        $playButton.on("click", function () {
            state();
        });

    };
});