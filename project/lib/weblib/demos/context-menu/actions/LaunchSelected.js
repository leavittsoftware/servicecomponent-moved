﻿BASE.require([
    'jQuery',
    'components.ui.layouts.UIContextMenuActionBehavior'
], function () {
    BASE.namespace("demos.actions");

    demos.actions.LaunchSelected = function (elem, tags, context) {
        var self = this;
        var $elem = $(elem);

        components.ui.layouts.UIContextMenuActionBehavior.call(self, elem);

        var canLaunch = function (item) {
            return item.type === "Spaceship";
        };

        self.isApplicableForItems = function (items) {
            return items.every(canLaunch);
        };
    };
});