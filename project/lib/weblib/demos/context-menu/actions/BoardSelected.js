﻿BASE.require([
    'jQuery',
    'components.ui.layouts.UIContextMenuActionBehavior'
], function () {
    BASE.namespace("demos.actions");

    demos.actions.BoardSelected = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);

        components.ui.layouts.UIContextMenuActionBehavior.call(self, elem);

        var carriesPeople = function (item) {
            return (["Car", "Airplane", "Boat", "Spaceship"].indexOf(item.type) > -1);
        };

        self.isApplicableForItems = function (items) {
            return items.every(carriesPeople);
        };

    }
});