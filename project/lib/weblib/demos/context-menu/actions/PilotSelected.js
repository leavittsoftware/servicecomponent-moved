﻿BASE.require([
    'jQuery',
    'components.ui.layouts.UIContextMenuActionBehavior'
], function () {
    BASE.namespace("demos.actions");

    demos.actions.PilotSelected = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);

        components.ui.layouts.UIContextMenuActionBehavior.call(self, elem);

        var flies = function (item) {
            return item.altitude > 0;
        };

        self.isApplicableForItems = function (items) {
            return items.every(flies);
        };

    };
});