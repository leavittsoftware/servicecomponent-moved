﻿BASE.require([
    "jQuery"
], function () {
    BASE.namespace("demos");

    demos.VehicleItem = function (elem, tags, scope) {
        var $elem = $(elem);
        var self = this;

        var $label = $(tags['label']);
        var $fuel = $(tags['fuel']);
        var $altitude = $(tags['altitude']);
        var $type = $(tags['type']);
        var $select = $(tags['select']);

        
        self.vehicle = null;

        self.setVehicle = function (value) {
            self.vehicle = value;
            $label.text(value.label);
            $fuel.text(value.fuel);
            $altitude.text(value.altitude);
            $type.text(value.type);
        }

        self.setChecked = function (state) {
            if (state) {
                $(elem).addClass("checked");
            } else {
                $(elem).removeClass("checked");
            }
        };

        $select.on("click", function (event) {
            event.stopPropagation();
            var selectEvent = new jQuery.Event("select");
            selectEvent.vehicle = self.vehicle;
            selectEvent.card = elem;
            $elem.trigger(selectEvent);
        });

        $elem.on("click", function (event) {
            event.stopPropagation();
            var chooseEvent = new jQuery.Event("choose");
            chooseEvent.vehicle = self.vehicle;
            chooseEvent.card = elem;
            $elem.trigger(chooseEvent);
        });

    };
});