﻿BASE.require([
    'jQuery',
    'Array.prototype.asQueryable',
    'demos.vehicles',
    'components.ui.layouts.collections.GridLayout',
    'BASE.collections.Hashmap',
    'BASE.async.Future'
], function () {
    BASE.namespace("demos");
    var Future = BASE.async.Future;

    demos.DemoMain = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);

        var $contextMenu = $(tags['contextMenu']);
        var contextMenu = $contextMenu.controller();
        var $collection = $(tags['collection']);
        var collection = $collection.controller();

        var selectedItemsHash = new BASE.collections.Hashmap();
        var contextMenuFuture = Future.fromResult(null);

        var layout = new components.ui.layouts.collections.GridLayout({ width: 200, height: 100, paddingTop: 10, paddingBottom: 10 });
        layout.component = "VehicleItem.html";

        layout.prepareElement = function (element, item, index) {
            var controller = $(element).controller();
            controller.setVehicle(item);
        };

        layout.cleanElement = function (element) {
            var controller = $(element).controller();
            controller.setChecked(false);
        };

        collection.setLayout(layout);
        collection.setQueryable(demos.vehicles.asQueryable());

        $elem.on("select", function (e) {
            if (selectedItemsHash.hasKey(e.vehicle)) {
                $(e.card).controller().setChecked(false);
                selectedItemsHash.remove(e.vehicle, e.card);
            } else {
                $(e.card).controller().setChecked(true);
                selectedItemsHash.add(e.vehicle, e.card);
            }

            updateContextMenu(selectedItemsHash.getKeys());
        });

        var itemToLabel = function (item) { return item.label; };

        var actionComponentsToHandlers = {
            "actions/PilotSelected.html": function (event) {
                console.log("Piloting: ");
                console.log(selectedItemsHash.getKeys().map(itemToLabel));
            },
            "actions/LaunchSelected.html": function (event) {
                console.log("Launching: ");
                console.log(selectedItemsHash.getKeys().map(itemToLabel));
            },
            "actions/BoardSelected.html": function (event) {
                console.log("Boarding: ");
                console.log(selectedItemsHash.getKeys().map(itemToLabel));
            }
        };
        var setUpContextMenuActions = function () {
            contextMenu.setItemContainerHeight("61px");
            contextMenu.setItemContainerPadding("0 5px 0 5px");
            contextMenu.setContentContainerPadding("0, 20px 0 20px");

            Object.keys(actionComponentsToHandlers).forEach(function (componentName) {
                BASE.web.components.createComponent(componentName).then(function (component) {
                    $(component).on("click", actionComponentsToHandlers[componentName]);
                    contextMenu.addAction(component);
                });
            });
        };
        
        var updateContextMenu = function (items) {
            contextMenuFuture.cancel();

            if (items.length > 0) {
                contextMenuFuture = contextMenu.show();
            } else {
                contextMenuFuture = contextMenu.hide();
            }

            contextMenu.selectActionsForItems(items);
        };


        setUpContextMenuActions();
    };

});