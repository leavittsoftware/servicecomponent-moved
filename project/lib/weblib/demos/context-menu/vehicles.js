﻿(function () {

    BASE.namespace("demos");
    demos.vehicles = [
        { label: "Space Shuttle", altitude: 2034120, fuel: "H + O2", type: "Spaceship" },
        { label: "Chevy Volt", altitude: 0, fuel: "Electricity, Gasoline", type: "Car" },
        { label: "Piper Cheroke", altitude: 14300, fuel: "Avgas", type: "Airplane" },
        { label: "Yacht", altitude: 0, fuel: "Gasoline", type: "Boat" },
        { label: "Drone", altitude: 150, fuel: "Electricity", type: "Drone"}
    ];

})();