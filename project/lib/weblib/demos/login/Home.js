﻿BASE.require([
    "jQuery",
    "components.material.login.AuthenticationResult"
], function () {
    BASE.namespace('demos.login');

    var AuthenticationResult = components.material.login.AuthenticationResult;

    demos.login.Home = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $login = $(tags['login']);
        var loginController = $login.controller();
        var $stateManager = $(tags['state-manager']);
        var stateManagerController = $stateManager.controller();
        var localStorageKey = "authenticatedUserData";
        var requiredAppPermissions = ["Sales Landscape Report Viewer"];

        var authenticateAsync = function () {

            var lastValidAuthResult = self.getLastValidAuthResult();

            //pre-fill username and password from last user logged in...
            loginController.setLastLoggedIn(lastValidAuthResult);

            //Do some auth
            return loginController.authenticateAsync(lastValidAuthResult.token, requiredAppPermissions)
                .chain(function (authResult) {
                    authResult.saveToLocalStorage(localStorageKey);
                   /// ajaxConfig.headers["Authorization"] = "Bearer " + self.getLastValidAuthResult().token;
                });
        }

        self.getLastValidAuthResult = function () {
            return AuthenticationResult.fromLocalStorage(localStorageKey);
        }

        self.init = function () {
            authenticateAsync().chain(function () {
                //PUT Initial state that you want a user to land after logging in or returning to the site with a valid token
            })["try"]();
        };

        self.logout = function () {

            //PUSH TO LOGIN CONTROLLER STATE

            new AuthenticationResult().saveToLocalStorage(localStorageKey);

            loginController.showLoggedOutStateWaitForRetryAsync().chain(function () {
                return authenticateAsync();
            }).chain(function () {
                //This state should be where you want the user to land after logging out and re-logging in
            })["try"]();

        };

        $elem.on("session-expired", function () {

            //Save current app state
        
            //clear the token from local storage
            var lastValidAuthResult = self.getLastValidAuthResult();
            lastValidAuthResult.clearToken();
            lastValidAuthResult.saveToLocalStorage(localStorageKey);

            loginController.showSessionExpiredStateWaitForRetryAsync().chain(function () {
                return authenticateAsync();

            }).chain(function () {
                // This state should return the user to where he/she were prior to token expiration.
                // PUSH BACK TO STATE USER WAS AT HERE
            })["try"]();
        });

        $elem.on("logout", function () {
            self.logout();
        });

        self.init();
    }
});