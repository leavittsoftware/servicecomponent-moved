﻿BASE.require([
    'jQuery',
    'BASE.speech.WebSpeechRecognition',
    'BASE.speech.WebSpeechSynthesis'
], function () {
    BASE.namespace('speech');

    var Future = BASE.async.Future;
    var WebSpeechSynthesis = BASE.speech.WebSpeechSynthesis;
    var WebSpeechRecognition = BASE.speech.WebSpeechRecognition;

    speech.AppMain = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var longTestTextareaController = $(tags['long-test-textarea']).controller();
        var $longTextMic = $(tags['long-test-mic']);
        var $longTestStop = $(tags['long-test-stop']);
        var recognitionFuture = Future.fromResult();
        var $speechSynthesisTest = $(tags['speech-synthesis-test']);
        var $questionAnswerTest = $(tags['question-answer-test']);


        var synthesis = new WebSpeechSynthesis();
        var recognition = new WebSpeechRecognition();

        var longTestMicClickHander = function () {
            recognition.continuous = true;
            recognition.interimResults = true;
            recognitionFuture = recognition.recognitionAsync().chain(function () {
                console.log('ended');
            }).catchCanceled(function () {
                console.log('canceled');
            })["try"]();

            var value = longTestTextareaController.getValue();

            recognition.observeType('interimSpeechResult', function (event) {
                console.log('interim speech');
                longTestTextareaController.setValue(value + event.value);
            });

            recognition.observeType('finalSpeechResult', function (event) {
                console.log('final speech');
                value += event.value;
                longTestTextareaController.setValue(value);
            });
        };

        var longTestStopClickHandler = function () {
            recognitionFuture.cancel();
        };

        synthesis.getVoicesAsync().chain(function (voices) {
            filteredVoices = voices.filter(function (voice) {
                return voice.lang === "en-GB";
            });

            synthesis.voice = filteredVoices[0];
        })["try"]();

     

        var speechSynthesisTestClickHandler = function () {
            var firstFuture = synthesis.speakAsync('hello world').then(function () {
                synthesis.speakAsync('I am last').try();
            }).ifCanceled(function(){
                synthesis.speakAsync('I was canceled first')["try"]();
            });
            var future = synthesis.speakAsync('I am next')["try"]().ifCanceled(function () {
                synthesis.speakAsync('I am canceled')["try"]();
            });

            setTimeout(function () {
                future.cancel();
            }, 500);

        };

        var questionAnswerTestClickHandler = function () {
            recognition.continuous = false;
            recognition.interimResults = false;
            
            var speechResult = null;

            var synthesisFuture = synthesis.speakAsync('What is the capital of Utah').chain(function () {
                return recognition.recognitionAsync().chain(function (results) {
                    console.log(results);
                    if (results[0].toLowerCase() === 'salt lake city') {
                        synthesis.speakAsync('That is correct!')["try"]();
                    } else {
                        synthesis.speakAsync('I\'m sorry that is wrong')["try"]();
                    }
                });
            })["try"]();
        };

        $longTextMic.on('click', longTestMicClickHander);
        $longTestStop.on('click', longTestStopClickHandler);
        $speechSynthesisTest.on('click', speechSynthesisTestClickHandler);
        $questionAnswerTest.on('click', questionAnswerTestClickHandler);
    };
});