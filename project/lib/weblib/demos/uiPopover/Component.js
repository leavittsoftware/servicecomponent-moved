﻿BASE.require([
    "jQuery",
    "components.ui.layouts.UIPopoverBehavior"
], function () {
    BASE.namespace('demos.uiPopover');

    demos.uiPopover.Component = function (elem, tags, scope) {
        var self = this;

        var $elem = $(elem);
        var $top = $(tags['top']);
        var $bottom = $(tags['bottom']);
        var $left = $(tags['left']);

        self.createPopover = function (element, options) {
            components.ui.layouts.UIPopoverBehavior.call({}, element);
            return $(element).data('popover').createPopover(options);
        }

        self.createPopover($top[0], {
            title: '<h3 class="no-margin">hello world</h3>',
            content: '<p>Whats up! I am a cool popover thing. I want to see when I wrap.</p>',
            placement: 'top'
        }).then(function (popoverManager) {
            popoverManager.setMaxWidth(300);

            $top.on('focus', popoverManager.show);

            $top.on('blur', popoverManager.hide);
        });

        self.createPopover($bottom[0]).then(function (popoverManager) {
            $bottom.on('focus', function () {
                popoverManager.setOptionsAsync({
                    hideArrow: true,
                    title: '<h3 class="no-margin">I\'m dynamic</h3>',
                    content: '<p>I\'m kind of a big deal.</p>'
                });
                popoverManager.show().then(function () {
                    var region = popoverManager.getRegion();
                    var elemBottom = region.elem.top + region.elem.height;
                    var elemLeft = region.elem.left;
                    // also have the option to get the popover region with region.popover
                    popoverManager.setPosition(elemBottom + 20, elemLeft);
                });
            });
               
            $bottom.on('blur', popoverManager.hide);
        });

        self.createPopover($left[0], {
            title: '<h3 class="no-margin">hello world</h3>',
            component: 'ui-alert',
            componentContent: '<h3>I am an alert</h3>',
            componentAttributes: {"class":"danger", "hide-close": ""},
            placement: 'left'
        }).then(function (popoverManager) {
            $left.on('focus', popoverManager.show);

            $left.on('blur', popoverManager.hide);
        });
        
       
    }
});