﻿BASE.require([
    "jQuery",
    "components.material.segues.FadeOutFadeIn"
], function () {
    var FadeOutFadeIn = components.material.segues.FadeOutFadeIn;
    var segue = new FadeOutFadeIn();

    BASE.namespace("uiStateManagerDemo");

    uiStateManagerDemo.AppMain = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $stateName = $(tags["state-name"]);
        var $stateManager = $(tags["state-manager"]);
        var $push = $(tags["push"]);
        var $pop = $(tags["pop"]);
        var stateManagerController = $stateManager.controller();

        $pop.on("click", function () {
            stateManagerController.popAsync({
                segue: segue
            })["try"]();
        });

        $push.on("click", function () {
            var stateName = $stateName.val();
            stateManagerController.pushAsync(stateName, {
                segue: segue
            })["try"]();
        });
    };

});