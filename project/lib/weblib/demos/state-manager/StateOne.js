﻿BASE.require([
    "jQuery"
], function () {
    BASE.namespace("StateManagerDemo");

    StateManagerDemo.StateOne = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);

        self.prepareToActivateAsync = function (options) {
            $elem.css("background-color", options.color || "red");
        };
    };

});