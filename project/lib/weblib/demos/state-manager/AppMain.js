﻿BASE.require([
    "jQuery",
    "components.material.segues.FadeOutFadeIn",
    "components.material.segues.AppearInstantSegue",
    "components.material.segues.SlideLeftToRightSoft",
    "components.material.segues.SlideRightToLeftSoft",
    "components.material.segues.ZoomOutFadeIn",
    "components.material.segues.InertToFadeIn",
    "components.material.segues.FadeOutToInert"
], function () {
    var fadeOutFadeIn = new components.material.segues.FadeOutFadeIn();
    var appearInstantSegue = new components.material.segues.AppearInstantSegue();
    var slideLeftToRightSoft = new components.material.segues.SlideLeftToRightSoft();
    var slideRightToLeftSoft = new components.material.segues.SlideRightToLeftSoft();
    var zoomOutFadeIn = new components.material.segues.ZoomOutFadeIn();
    var InertToFadeIn = new components.material.segues.InertToFadeIn();
    var FadeOutToInert = new components.material.segues.FadeOutToInert();

    var pushSegue = slideRightToLeftSoft;
    var popSegue = slideLeftToRightSoft;

    BASE.namespace("StateManagerDemo");

    StateManagerDemo.AppMain = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $stateName = $(tags["state-name"]);
        var $stateManager = $(tags["state-manager"]);
        var $push = $(tags["push"]);
        var $replace = $(tags["replace"]);
        var $pop = $(tags["pop"]);
        var stateManagerController = $stateManager.controller();
        var $footer = $(tags['footer']);
        var $jamThread = $(tags["jam-thread"]);
        var threeController = $(tags['three']).controller();

        threeController.getChildElementAsync().then(function (child) {
            var controller = $(child).controller();
            controller.setValue();
        });

        var jamThread = function (duration) {
            var startTime = Date.now();
            var elapsedTime = 0;

            while (elapsedTime < duration) {
                elapsedTime = Date.now() - startTime;
            }
        };

        stateManagerController.pushAsync('one', { segue: pushSegue }).then(function () {
            $footer.removeClass('hide');
        });

        $pop.on("click", function () {
            stateManagerController.popAsync({
                segue: popSegue
            })["try"]();
        });

        $push.on("click", function () {
            var stateName = $stateName.val();
            stateManagerController.pushAsync(stateName, {
                segue: pushSegue
            })["try"]();
        });

        $replace.on("click", function () {
            var stateName = $stateName.val();
            stateManagerController.replaceAsync(stateName, {
                segue: pushSegue
            })["try"]();
        });

        $stateManager.on("stateChange", function (e) {
            console.log(e);
        });

        $jamThread.on("click", function () {
            jamThread(4000);
        });

        window.stateManager = stateManagerController;
        window.popSegue = popSegue;
    };

});