﻿BASE.require([
    "jQuery"
], function () {
    BASE.namespace('demos.twoColumnLeftOverlay');

    demos.twoColumnLeftOverlay.Component = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $menuButton = $(tags['menu-button']);
        var twoColumnLeftOverlayController = $(tags['two-column-left-overlay']).controller();

        $menuButton.on('click', function () {
            twoColumnLeftOverlayController.showLeftColumn();
        });

       
    }
});