﻿BASE.require([
    "jQuery",
    "BASE.web.animation.ElementAnimation"
], function () {

    BASE.namespace("demos");

    var ElementAnimation = BASE.web.animation.ElementAnimation;

    demos.ListItem = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $nameText = $(tags["nameText"]);
        var $deleteButton = $(tags["deleteButton"]);
        var state;

        var expansionAnimation = new ElementAnimation({
            target: elem,
            easing: "easeOutExpo",
            properties: {
                height: {
                    from: "80px",
                    to: "200px"
                },
                backgroundColor: {
                    from: "#ADD8E6",
                    to: "#008000"
                }
            },
            duration: 600
        });

        var expandedRemovalAnimation = new ElementAnimation({
            target: elem,
            easing: "easeOutExpo",
            properties: {
                height: {
                    from: "200px",
                    to: "0px"
                },
                paddingTop: {
                    from: "16px",
                    to: "0px"
                },
                paddingBottom: {
                    from: "16px",
                    to: "0px"
                },
                opacity: {
                    from: "100%",
                    to: "0%"
                },
                backgroundColor: {
                    from: "#008000",
                    to: "#ffffff"
                }
            },
            duration: 600
        });

        var closedRemovalAnimation = new ElementAnimation({
            target: elem,
            easing: "easeOutExpo",
            properties: {
                height: {
                    from: "80px",
                    to: "0px"
                },
                paddingTop: {
                    from: "16px",
                    to: "0px"
                },
                paddingBottom: {
                    from: "16px",
                    to: "0px"
                },
                opacity: {
                    from: "100%",
                    to: "0%"
                },
                backgroundColor: {
                    from: "#ADD8E6",
                    to: "#ffffff"
                }
            },
            duration: 600
        });

        var unselected = function () {
            state = selected;
            expansionAnimation.play();
        };

        var selected = function () {
            state = unselected;
            expansionAnimation.reverse();
        };

        state = unselected;

        self.setItem = function (item, index) {
            $nameText.text(item.firstName + " " + item.lastName);
        };

        $elem.on("click", function () {
            state(event);
        });

        $deleteButton.on("click", function (e) {
            $elem.empty();
            $elem.css("border", "0");
            if (state === selected) {
                expandedRemovalAnimation.play();
            } else {
                closedRemovalAnimation.play();
            }
            e.stopPropagation();
        });

    };

});