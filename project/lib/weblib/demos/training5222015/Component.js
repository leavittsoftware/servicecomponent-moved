﻿BASE.require([
    "jQuery"
], function () {
    BASE.namespace('demos.training5222015');

    demos.training5222015.Component = function (elem, tags, scope) {
        var self = this;
        var $elem = $(elem);
        var $menuButton = $(tags['menu-button']);
        var twoColumnLeftOverlayController = $(tags['two-column-left-overlay']).controller();
        var listController = $(tags['list']).controller();

        // Create an array to make a queryable with.
        var list = [{
            firstName: "Jared",
            lastName: "Barnes"
        },
        {
            firstName: "Justin",
            lastName: "Barnes"
        },
        {
            firstName: "LeAnn",
            lastName: "Barnes"
        },
        {
            firstName: "Ben",
            lastName: "Howe"
        },
        {
            firstName: "Ashley",
            lastName: "Howe"
        }];

        $menuButton.on('click', function () {
            twoColumnLeftOverlayController.showLeftColumn();
        });

        listController.getLayout().prepareElement = function (item, entity, index) {
            var $item = $(item);
            var itemController = $(item).controller();

            itemController.setValue(entity, entity.firstName + ' ' + entity.lastName);
        };

        listController.setQueryable(list.asQueryable());   
    }
});