BASE.require([
],function(){

    var RATING_SCALE = 400;
    var DEFAULT_RANK = 1000;

    BASE.namespace("app.util");

    app.util.EloService = function(playersValue){
	var self=this;
	var players=playersValue;
	
	KFactorStates = {
	    "ICC":{getK:function(playerA,playerB){
		if(playerA.eloRank < 2100 || playerB.eloRank < 2100){
		    return 32;
		}else if(playerA.eloRank < 2400 || playerB.eloRank < 2400){
		    return 24;
		}else{
		    return 16;
		}
	    }
	    },
	    "FIDE":{getK:function(playerA,playerB){
		playerAGames = playerA.wins + playerA.losses + playerA.draws;
		playerBGames = playerB.wins + playerB.losses + playerB.draws;

		if(playerAGames < 30 || playerBGames < 30){
		    return 40;
		}else if(playerA.eloRank < 2400 || playerB.eloRank < 2400){
		    return 20;
		}else{
		    return 10;
		}
	    }
	    }
	};
	
	KFactorState = KFactorStates['ICC'];
	
	self.getExpectedScores = function(playerA,playerB){
	    var numeratorA = Math.pow(10,playerA.eloRank/RATING_SCALE);
	    var numeratorB = Math.pow(10,playerB.eloRank/RATING_SCALE);
	    var denominator = numeratorA + numeratorB;
	    return [numeratorA/denominator,numeratorB/denominator];
	};

	self.updateRankings = function(playerA,playerAWins,playerB,playerBWins){
	    var gamesPlayed = playerAWins + playerBWins;
	    var playerAScore = playerAWins / gamesPlayed;
	    var playerBScore = playerBWins / gamesPlayed;
	    var expectedScores = self.getExpectedScores(playerA,playerB);
	    var playerAExpected = expectedScores[0];
	    var playerBExpected = expectedScores[1];
	    var K = KFactorState.getK(playerA,playerB);
	    playerA.eloRank = playerA.eloRank + K*(playerAScore - playerAExpected);
	    playerB.eloRank = playerB.eloRank + K*(playerBScore - playerBExpected);
	};

	self.initializePlayer = function(player){
	    player.eloRank = DEFAULT_RANK;
	}
	
    };
    
});
