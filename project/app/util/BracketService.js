BASE.require([
    'app.model.Player'
],function(){

    BASE.namespace("app.util");
    var Player = app.model.Player;

    app.util.BracketService = function(serviceValue){
	var self=this;
	var service = serviceValue;
	
	self.updateRankings = function(playerA,playerAWins,playerB,playerBWins){
	    var winner = playerAWins > playerBWins ? playerA : playerB;
	    var loser = playerAWins > playerBWins ? playerB : playerA;
	    if(app.util.BracketService.getYPosition(winner.bracketRank) - 1 === app.util.BracketService.getYPosition(loser.bracketRank)){
		playerABracketRank = playerA.bracketRank;
		playerA.bracketRank = playerB.bracketRank;
		playerB.bracketRank = playerABracketRank;
	    }
	};

	self.initializePlayerAsync = function(player){
	    return service.asQueryable(Player).orderByDesc(function(expBuilder){
		return expBuilder.property('bracketRank');
	    }).toArray(function(players){
		if(players.length > 0){
		    player.bracketRank = players[0].bracketRank + 1;
		}else{
		    player.bracketRank = 0;
		}
	    });
	};
	
    };

    app.util.BracketService.getYPosition = function(index){
	return Math.floor(Math.log2(index+1));
    };

    app.util.BracketService.getXPosition = function(index,y){
	var row = Math.pow(2,y);
	return (100/(row+1))*(index + 2 - row);
    };
});
