BASE.require([
    'jQuery'
],function(){
    BASE.namespace('app.components.layouts');

    app.components.layouts.PlayerItem = function(elem,tags,services){
	var self = this;
	var $elem = $(elem);
	var $name = $(tags['name']);
	var $points = $(tags['points']);

	var player;

	self.setItem = function(item){
	    player = item;
	    $name.text(player.name);
	    $points.text(player.eloRank.toFixed(0));
	};

	self.getItem = function(){
	    return player;
	};

	self.setColor = function(minValue,maxValue){
	    var colorValue = (player.eloRank - minValue)/(maxValue-minValue);
	    //if(colorValue.length === 1){
		//colorValue = '0' + colorValue;
	    //}
	    $elem.css({'background-color':'rgba(255,255,0,'+colorValue+')'});
	};

	var clickHandler = function(event){
	    event.stopPropagation();
	    if($elem.hasClass('selected')){
		$elem.removeClass('selected');
	    }else{
		$elem.addClass('selected');
	    }
	    $elem.trigger({
		type:'playerSelected',
		player:player
	    });
	};

	$elem.on('click',clickHandler);

    };
})
