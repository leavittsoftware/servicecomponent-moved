BASE.require([
    'jQuery',
    'app.util.EloService',
    'app.util.BracketService',
    'components.material.segues.FadeOutFadeIn',
    'BASE.data.DataContext',
    'BASE.async.Future',
    'app.model.Player',
    'youtubeApi'
],function(){
    BASE.namespace('app.components.states');
    var EloService = app.util.EloService;
    var BracketService = app.util.BracketService;
    var fadeOutFadeIn = new components.material.segues.FadeOutFadeIn();
    var DataContext = BASE.data.DataContext;
    var Future = BASE.async.Future;
    var Player = app.model.Player;

    app.components.states.AddMatchState = function(elem,tags,services){
	var self = this;
	var $elem = $(elem);
	var $playerAName = $(tags['player-a-name']);
	var $playerBName = $(tags['player-b-name']);
	var playerAWinsController = $(tags['player-a-wins']).controller();
	var playerBWinsController = $(tags['player-b-wins']).controller();
	var $cancelButton = $(tags['cancel-button']);
	var $saveButton = $(tags['save-button']);

	var playerA = null;
	var playerB = null;
	var playerAYoutubeController;
	var playerBYoutubeController;

	function onYouTubeIframeAPIReady() {
	    player = new YT.Player('player', {
		height: '390',
		width: '640',
		videoId: 'M7lc1UVf-VE',
		events: {
		    'onReady': onPlayerReady,
		    'onStateChange': onPlayerStateChange
		}
	    });
	};
	
	function onPlayerReady(event) {
	    event.target.playVideo();
	}
	
	var done = false;
	function onPlayerStateChange(event) {
	    if (event.data == YT.PlayerState.PLAYING && !done) {
		setTimeout(stopVideo, 6000);
		done = true;
	    }
	}
	
	function stopVideo() {
	    player.stopVideo();
	}
	
	
	var cancelHandler = function(event){
	    self.stateManager.popAsync({segue:fadeOutFadeIn}).try();
	};

	var validateAsync = function(){
	    var futures = [];
	    futures.push(playerAWinsController.validate());
	    futures.push(playerBWinsController.validate());
	    return Future.all(futures);
	};

	var saveHandler = function(event){
	    var eloService = new EloService();
	    var bracketService = new BracketService(services.get('service'));

	    validateAsync().chain(function(){
		var service = services.get('service');
		//dataContext.attachEntity(playerA);
		//dataContext.attachEntity(playerB);
		var playerAWins = parseInt(playerAWinsController.getValue(),10);
		var playerBWins = parseInt(playerBWinsController.getValue(),10);
		playerA.wins = (playerA.wins||0) + playerAWins;
		playerB.wins = (playerB.wins||0) + playerBWins;
		playerA.losses = (playerA.losses||0) + playerBWins;
		playerB.losses = (playerB.losses||0) + playerAWins;
		eloService.updateRankings(playerA,playerAWins,playerB,playerBWins);
		bracketService.updateRankings(playerA,playerAWins,playerB,playerBWins);
		service.update(Player, playerA, playerA);
		service.update(Player, playerB, playerB);
	    }).chain(function(){
		return self.stateManager.popAsync({segue:fadeOutFadeIn});
	    }).try();
	};

	self.init = function(stateManager){
	    self.stateManager = stateManager;
	};

	self.prepareToActivateAsync = function(options){
	    playerA = options.playerA;
	    playerB = options.playerB;

	    playerAWinsController.setValue('');
	    playerBWinsController.setValue('');
	    $playerAName.text(playerA.name);
	    $playerBName.text(playerB.name);
	};

	
	$cancelButton.on('click',cancelHandler);
	$saveButton.on('click',saveHandler);	
	youtubeApi.addObserver(onYouTubeIframeAPIReady);
    };
})
