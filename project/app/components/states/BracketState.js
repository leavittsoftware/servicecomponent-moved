BASE.require([
    'jQuery',
    'BASE.web.components',
    'BASE.async.Future',
    'components.material.segues.FadeOutFadeIn',
    'app.util.BracketService',
    "BASE.web.animation.ElementPathAnimation",
    "BASE.web.animation.Timeline"

],function(){
    BASE.namespace('app.components.states');

    var fadeOutFadeIn = new components.material.segues.FadeOutFadeIn();
    var baseComponents = BASE.web.components;
    var Future = BASE.async.Future;
    var BracketService = app.util.BracketService;
    var ElementPathAnimation = BASE.web.animation.ElementPathAnimation;
    var Timeline = BASE.web.animation.Timeline;

    app.components.states.BracketState = function(elem,tags,services){
	var self = this;
	var $elem = $(elem);
	var selectedPlayer = null;
	var items = [];

	var isPowerOfTwo = function(n){
	    return n !== 0 && ((n & (n-1)) === 0);
	}

	var playerSelectedHandler = function(event){
	    var player = event.player;
	    if(selectedPlayer === null){
		selectedPlayer = player;
	    }else if(selectedPlayer === player){
		selectedPlayer = null;
	    }else{
		self.stateManager.pushAsync('add-match-state',{segue:fadeOutFadeIn,playerA:selectedPlayer,playerB:player}).try();
	    }
	};

	var refreshItemsAsync = function(players){
	    $elem.empty();
	    var futures = players.map(function(player,index){
		return baseComponents.createComponent('player-item').chain(function(addedItem){
		    var $itemElement = $(addedItem);
                    var itemElementController = $itemElement.data("controller");
		    itemElementController.setItem(player);
		    $elem.append($itemElement);
		    return addedItem;
		});

	    });
	    return Future.all(futures).chain(function(itemsValue){
		items = itemsValue;
	    });
	};

	var XSCALE = 15;
	var YSCALE = 130;
	var DURATION = 2000;

	var refreshPlayersPositionsAsync = function(minRank,maxRank){
	    var timeline = new Timeline(DURATION);
		
	    items.forEach(function(item,index){
		var y = BracketService.getYPosition(index);
		var x = BracketService.getXPosition(index,y);
		
		var controller = $(item).controller();
		var player = controller.getItem();
		controller.setColor(minRank,maxRank);
		var animation = new ElementPathAnimation({
		    target: item,
		    unit: "%",
		    easing: 'easeOutSine',
		    from: { x:player.x||0 , y: player.y||0 },
		    to: { x: x*XSCALE, y: y*YSCALE},
		    duration: DURATION
		});
		player.x = x * XSCALE;
		player.y = y * YSCALE;
		timeline.add({
		    animation:animation,
		    startAt:0,
		    endAt:DURATION,
		    offset:0
		});
		
		//$(item).css({top: y*100, left: x+'%'});
		
	    });
	    return timeline.play();
	};
	     
	self.init = function(parentStateManager){
	    self.stateManager = parentStateManager;
	}
	
	self.prepareToActivateAsync = function(options){
	    selectedPlayer = null;
	    var players = services.get('players').orderBy(function(player){
		return player.eloRank;
	    });

	    var minRank = players[0].eloRank;
	    var maxRank = players[players.length - 1].eloRank;

	    players = players.orderBy(function(player){
		return player.bracketRank;
	    });
	    
	    var future;
	    //if(options && options.refreshItems){
		future = refreshItemsAsync(players);
	    //}else{
		//future = Future.fromResult();
	    //}
	    
	    return future.chain(function(){
		return refreshPlayersPositionsAsync(minRank,maxRank);
	    });

	};

	$elem.on('playerSelected',playerSelectedHandler);
    };
})
