BASE.require([
    'jQuery',
    'app.model.Player',
    'app.util.EloService',
    'app.util.BracketService',
    'components.material.segues.FadeOutFadeIn',
    'BASE.data.DataContext',
], function () {
    BASE.namespace('app.components.states');
    var Player = app.model.Player;
    var EloService = app.util.EloService;
    var BracketService = app.util.BracketService;
    var fadeOutFadeIn = new components.material.segues.FadeOutFadeIn();
    var DataContext = BASE.data.DataContext;

    app.components.states.AddPlayerState = function (elem, tags, services) {
        var self = this;
        var $elem = $(elem);
        var playerFormController = $(tags['player-form']).controller();
        var $cancelButton = $(tags['cancel-button']);
        var $saveButton = $(tags['save-button']);

        var player;

        var initializePlayerAsync = function (player) {
            var eloService = new EloService();
            var bracketService = new BracketService(services.get('service'));
            eloService.initializePlayer(player);
            return bracketService.initializePlayerAsync(player).chain(function () {
                return player;
            });
        };

        var cancelHandler = function (event) {
            self.stateManager.popAsync({ segue: fadeOutFadeIn }).try();
        };

        var saveHandler = function (event) {
            var service = services.get('service');

            playerFormController.validateAsync().ifError(function () {

            }).chain(function () {
                playerFormController.updatePlayer(player);
                return initializePlayerAsync(player);
            }).chain(function () {
                service.add(Player,player);
                services.get('players').push(player);
            }).chain(function () {
                return self.stateManager.popAsync({
                    segue: fadeOutFadeIn
                });
            }).catch(function (error) {

            }).try();
        };

        self.init = function (stateManager) {
            self.stateManager = stateManager;
        };

        self.prepareToActivateAsync = function (options) {
            player = new Player();
            playerFormController.setPlayer(player);
        };


        $cancelButton.on('click', cancelHandler);
        $saveButton.on('click', saveHandler);
    };
})
