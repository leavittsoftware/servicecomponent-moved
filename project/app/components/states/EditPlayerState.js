BASE.require([
    'jQuery',
    'app.model.Player',
    'app.util.EloService',
    'components.material.segues.FadeOutFadeIn',
    'BASE.data.DataContext',
], function () {
    BASE.namespace('app.components.states');
    var Player = app.model.Player;
    var EloService = app.util.EloService;
    var fadeOutFadeIn = new components.material.segues.FadeOutFadeIn();
    var DataContext = BASE.data.DataContext;

    app.components.states.EditPlayerState = function (elem, tags, services) {
        var self = this;
        var $elem = $(elem);
        var playerFormController = $(tags['player-form']).controller();
        var $cancelButton = $(tags['cancel-button']);
        var $saveButton = $(tags['save-button']);
        var $title = $(tags['title']);

        var player;

        var cancelHandler = function (event) {
            self.stateManager.popAsync({ segue: fadeOutFadeIn }).try();
        };

        var saveHandler = function (event) {

            playerFormController.validateAsync().chain(function () {
                var service = services.get('service');
                service.update(Player, player, player);
                playerFormController.updatePlayer(player);
            }).chain(function () {
                return self.stateManager.popAsync({ segue: fadeOutFadeIn });
            }).try();
        };

        self.init = function (stateManager) {
            self.stateManager = stateManager;
        };

        self.prepareToActivateAsync = function (options) {
            player = options.player;
            $title.text(player.name);
            playerFormController.setPlayer(player);
        };


        $cancelButton.on('click', cancelHandler);
        $saveButton.on('click', saveHandler);
    };
})

