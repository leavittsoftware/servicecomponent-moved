BASE.require([
    'jQuery',
    'components.material.segues.FadeOutFadeIn',
    'app.model.Player'
],function(){
    BASE.namespace('app.components.states');
    var fadeOutFadeIn = new components.material.segues.FadeOutFadeIn();
    var Player = app.model.Player;

    app.components.states.PlayerListState = function(elem,tags,services){
	var self = this;
	var $elem = $(elem);
	var playerListController = $(tags['player-list']).controller();
	var $addButton = $(tags['add-button']);

	var playerSelectedHandler = function(event){
	    event.stopPropagation();
	    self.stateManager.pushAsync('edit-player-state',{segue:fadeOutFadeIn,player:event.player}).try();
	};

	var addHandler = function(event){
	    event.stopPropagation();
	    self.stateManager.pushAsync('add-player-state',{segue:fadeOutFadeIn}).try();
	};

	self.init = function(stateManager){
	    self.stateManager = stateManager;
	};

	self.prepareToActivateAsync = function(options){
	    var service = services.get('service');
	    return playerListController.setQueryableAsync(service.asQueryable(Player));
	};

	$addButton.on('click',addHandler);

	$elem.on('playerSelected', playerSelectedHandler);	
    };
})
