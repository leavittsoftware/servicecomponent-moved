BASE.require([
    "BASE.async.Future"
], function () {

    var Future = BASE.async.Future;

    BASE.namespace("app.components.behaviors");

    app.components.behaviors.NumberGreaterThanEqualToZeroValidator = function (elem) {
        var self = this;

        var validateNumberGreaterThanEqualToZero = function (value) {
            var number = parseFloat(value);
            if (number >= 0) {
                return Future.fromResult();
            } else {
                return Future.fromError("Please enter a value greater than 0");
            }

        };

        self.registerValidator(validateNumberGreaterThanEqualToZero);

    };

});
