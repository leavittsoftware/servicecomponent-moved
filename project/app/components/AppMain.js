BASE.require([
    "jQuery",
    "components.material.segues.SlideRightToLeftSoft",
    "components.material.segues.FadeOutFadeIn",
    "BASE.data.DataContext",
    "BASE.data.databases.Sqlite",
    "BASE.data.services.SqliteService",
    "BASE.data.services.InMemoryService",
    "app.model.Edm",
    'app.model.Player'
], function () {
    BASE.namespace("app.components");

    var Sqlite = BASE.data.databases.Sqlite;
    var SqliteService = BASE.data.services.SqliteService;
    var InMemoryService = BASE.data.services.InMemoryService;
    var DataContext = BASE.data.DataContext;
    var Edm = app.model.Edm;
    var Player = app.model.Player;
    var fadeOutFadeIn = new components.material.segues.FadeOutFadeIn();

    app.components.AppMain = function (elem, tags, services) {
        var $elem = $(elem);
        var $stateManager = $(tags["state-manager"]);
        var stateManagerController = $stateManager.controller();
        var $hamburgerMenuButton = $(tags["hamburger-menu-button"]);
        var twoColumnLeftOverlayController = $(tags["two-column-left-overlay"]).controller();
        var $playersButton = $(tags["players-button"]);
        var $bracketButton = $(tags["bracket-button"]);

        self.init = function () {
            var edm = new Edm();

            var sqlite = new Sqlite({
                name: "tableTennis",
                edm: edm
            });

            var service = new SqliteService(sqlite);
            //var service = new SqliteService(edm);
            services.set("service", service);
            services.set("players", [
            //{name:"Anthony",eloRank:1000,bracketRank:0},
            //{name:"Blake",eloRank:1000,bracketRank:1},
            //{name:"Scotty",eloRank:1000,bracketRank:2},
            //{name:"Shawn",eloRank:1000,bracketRank:3},
            //{name:"Wyatt",eloRank:1000,bracketRank:4},
            //{name:"Rhett",eloRank:1000,bracketRank:5},
            //{name:"Jared",eloRank:1000,bracketRank:6},
            //{name:"Mike",eloRank:1000,bracketRank:7},
            //{name:"Ben",eloRank:1000,bracketRank:8},
            //{name:"Kasey",eloRank:1000,bracketRank:9},
            //{name:"Jesse",eloRank:1000,bracketRank:10},
            //{name:"Gavin",eloRank:1000,bracketRank:11},
            //{name:"Dustin",eloRank:1000,bracketRank:12},
            //{name:"Aaron",eloRank:1000,bracketRank:13},
            //{name:"Reese",eloRank:1000,bracketRank:14},
            //{name:"Bill Clinton",eloRank:1000,bracketRank:15}
            ]);

            service.asQueryable(Player).toArray(function (players) { 
                services.set("players",players);
            }).chain(function(){
                return stateManagerController.pushAsync('bracket-state', { segue: fadeOutFadeIn, refreshItems: true });
            }).try();
            
        };

        var hamburgerButtonHandler = function () {
            twoColumnLeftOverlayController.showLeftColumn();
        };

        var playerButtonHandler = function () {
            twoColumnLeftOverlayController.hideLeftColumn();
            stateManagerController.pushAsync('player-list-state', { segue: fadeOutFadeIn }).try();
        };

        var bracketButtonHandler = function () {
            twoColumnLeftOverlayController.hideLeftColumn();
            stateManagerController.pushAsync('bracket-state', { segue: fadeOutFadeIn, refreshItems: true }).try();
        };

        $hamburgerMenuButton.on('click', hamburgerButtonHandler);

        $playersButton.on('click', playerButtonHandler);

        $bracketButton.on('click', bracketButtonHandler);

        init();
    };
});
