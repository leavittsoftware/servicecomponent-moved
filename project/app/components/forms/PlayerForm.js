BASE.require([
    'jQuery',
    'BASE.async.Future'
],function(){
    BASE.namespace('app.components.forms');
    var Future = BASE.async.Future;
    
    app.components.forms.PlayerForm = function(elem,tags,services){
	var self = this;
	var $elem = $(elem);
	var $name = $(tags['name']);
	var nameController = $name.controller();

	self.setPlayer = function(player){
	    nameController.setValue(player.name);
	};

	self.updatePlayer = function(player){
	    player.name = nameController.getValue();
	}

	self.validateAsync = function(){
	    var futures = [];
	    futures.push(nameController.validate());
	    return Future.all(futures);
	};

    };
})
