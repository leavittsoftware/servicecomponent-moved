var assert = require('assert');
var path = require('path');

require("../../lib/weblib/lib/BASE/BASE.js");

BASE.require.loader.setRoot("./");
BASE.require.loader.setNamespace("app", path.resolve(__dirname,"../../../project/app/EloService/"));

exports["HappyTest"] = function () {
    assert(true);
}
BASE.require([
    'app.EloService'
], function () {
    
    exports["HappyTest"] = function (test) {
	test.ok(true);
	test.done();
    }

    exports["Expected Scores Test 1"] = function (test) {
	//Arrange
	var playerA = {eloRank:1000};
	var playerB = {eloRank:1000};
	var eloService = new app.EloService([playerA,playerB]);
	
	//Act
	var scores = eloService.getExpectedScores(playerA,playerB);
	
	//Assert
	test.equal(scores[0],.5,'playerA Expected Score');
	test.equal(scores[1],.5,'playerB Expected Score');
	test.done();
    }
    
    exports["Expected Scores Test 2"] = function (test) {
	//Arrange
	var playerA = {eloRank:1000};
	var playerB = {eloRank:1400};
	var eloService = new app.EloService([playerA,playerB]);
	
	//Act
	var scores = eloService.getExpectedScores(playerA,playerB);
	var sum = scores[1]+scores[0];
	
	//Assert
	test.equal(sum,1,'Expected Scores should sum to 1');
	test.done();
    }

    exports["Update Rankings Test 1"] = function (test) {
	//Arrange
	var playerA = {eloRank:500};
	var playerB = {eloRank:2000};
	var eloService = new app.EloService([playerA,playerB]);
	var initialSum = playerA.eloRank + playerB.eloRank;
	
	//Act
	eloService.updateRankings(playerA,2,playerB,0);
	
	//Assert
	test.equal(playerA.eloRank+playerB.eloRank,initialSum,'ranking should be 0 sum');
	test.done();
    }

});
