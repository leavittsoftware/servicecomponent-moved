require("../../lib/weblib/lib/BASE/BASE.js");
var path = require("path");

BASE.require.loader.setRoot("./");
BASE.require.loader.setNamespace("app", "../../../../app");

BASE.require([
    "BASE.testing.TestRunner",
    "BASE.testing.ConsoleOutput"
], function () {
    var testDirectory = path.resolve(__dirname, "../");

    var TestRunner = BASE.testing.TestRunner;
    var ConsoleOutput = BASE.testing.ConsoleOutput;
    debugger;
    var testRunner = new TestRunner(testDirectory);
    var consoleOutput = new ConsoleOutput(testRunner);
    testRunner.run();
});
