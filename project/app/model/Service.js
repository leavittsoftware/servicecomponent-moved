BASE.require([
    "app.model.Edm",
    "BASE.data.services.IndexedDbService"
], function () {

    var Edm = app.model.Edm;
    var InMemoryService = BASE.data.services.IndexedDbService;
    BASE.namespace("app.model");

    var Service = function (edm) {
        var self = this;

        InMemoryService.call(self, {edm:edm,name:"tableTennis"});	
    };

    BASE.extend(Service, InMemoryService);
    app.model.Service = Service;
});
