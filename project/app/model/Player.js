BASE.namespace("app.model");

app.model.Player = function(){
    var self = this;
    self.name = null;
    self.wins = null;
    self.losses = null;
    self.draws = null;
    self.eloRank = null;
    self.bracketRank = null;
};
