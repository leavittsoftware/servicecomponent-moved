BASE.require([
    "app.model.Player",
    "BASE.data.Edm"
], function () {

    var Edm = BASE.data.Edm;

    BASE.namespace("app.model");

    app.model.Edm = function () {

        var self = this;
        Edm.apply(self, arguments);

        self.addModel({
            type: app.model.Player,
            collectionName: "players",
            properties: {
                id: {
                    type: Integer,
                    primaryKey: true
                },
                name: {
                    type: String
                },
                wins: {
                    type: Integer
                },
                losses: {
                    type: Integer
                },
                draws: {
                    type: Integer
                },
                eloRank: {
                    type: Double
                },
                bracketRank: {
                    type: Integer
                }
            }
        });
        self.name = "tableTennis";

    };

    BASE.extend(app.model.Edm, Edm);


});

